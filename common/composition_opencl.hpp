/**
  Author: Arturo Argueta
  This file contains the main methods to do composition on a GPU
**/
#include <tuple>
#include <stack>
#include <CL/cl.hpp>

int ceildiv(int x, int y) { return (x-1)/y+1; }

int composed_index(int index_1,int index_2,int num_states){return index_1*num_states+index_2;}

int roundUp(int numToRound, int multiple)
{
    if (multiple == 0)
        return numToRound;

    int remainder = numToRound % multiple;
    if (remainder == 0)
        return numToRound;

    return numToRound + multiple - remainder;
}

#define MAX_SOURCE_SIZE (0x100000)
#define REPORT 0
inline void CheckError(cl_int error)
 {
  if (error != CL_SUCCESS) {
    std::cerr << "OpenCL call failed with error " << error << std::endl;
    std::exit (1);
  }
 }

/*
For this method, one kernel will be launched per expanded token given a source state pairs. The next method will launch 
one kernel per source state that wnts to be expanded
*/
void compute_efficient_composition(input_fst &m, output_fst &m2, composed_fst &test){

  // SETUP OPENCL ENVIRONMENT
  //cl::Context context;
  //std::vector<cl::Device> device;

  cl_device_id device_id = NULL;
  cl_context context = NULL;
  cl_command_queue command_queue = NULL;

  cl_mem m_to_states;
  cl_mem m_input;
  cl_mem m_prob;
  cl_mem m2_to_states;
  cl_mem m2_output;
  cl_mem m2_prob;

  cl_mem gpu_states;



  cl_program program = NULL;
  cl_kernel kernel = NULL;
  cl_platform_id platform_id = NULL;
  cl_uint ret_num_devices;
  cl_uint ret_num_platforms;
  cl_int ret;
 
 
  FILE *fp;
  char *source_str;
  size_t source_size;
 
  /* Load the source code containing the kernel*/
  fp = fopen("../common/kernels.cl", "r");
  if (!fp) {
    fprintf(stderr, "Failed to load kernel.\n");
    exit(1);
  }

  source_str = (char*)malloc(MAX_SOURCE_SIZE);
  source_size = fread(source_str, 1, MAX_SOURCE_SIZE, fp);
  fclose(fp);

  CheckError( clGetPlatformIDs(1, &platform_id, &ret_num_platforms));
  CheckError(clGetDeviceIDs(platform_id, CL_DEVICE_TYPE_DEFAULT, 1, &device_id, &ret_num_devices));

  // Create OpenCL context 
  context = clCreateContext(NULL, 1, &device_id, NULL, NULL, &ret);
  CheckError(ret); 
  // Create Command Queue 
  command_queue = clCreateCommandQueue(context, device_id, 0, &ret);
  CheckError(ret);

  // Create Memory Buffers
  int transitions = m.target_states.size();
  int transitions2 = m.target_states.size();
  int state_list_size = test.num_nodes;

  m_to_states = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR,  transitions * sizeof(int), &m.target_states[0], &ret);
CheckError(ret);
  m_input = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR,  transitions * sizeof(int), &m.inputs[0], &ret);
CheckError(ret);
  m_prob = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR,  transitions * sizeof(float), &m.probs[0], &ret);
CheckError(ret);

  m2_to_states = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR,  transitions2 * sizeof(int), &m2.target_states[0], &ret);
CheckError(ret);
  m2_output = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR,  transitions2 * sizeof(int), &m2.inputs[0], &ret);
CheckError(ret);
  m2_prob = clCreateBuffer(context, CL_MEM_COPY_HOST_PTR,  transitions2 * sizeof(float), &m2.probs[0], &ret);
CheckError(ret);


  gpu_states = clCreateBuffer(context, CL_MEM_READ_WRITE, state_list_size * sizeof(composed_fst_node), test.gpu_states, &ret);
  CheckError(ret);


  // Create Kernel Program from the source 
  program = clCreateProgramWithSource(context, 1, (const char **)&source_str, (const size_t *)&source_size, &ret);
   
  CheckError(ret);
  // Build Kernel Program
  CheckError( clBuildProgram(program, 1, &device_id, NULL, NULL, NULL));

  #if REPORT
    size_t log_size;
    CheckError( clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size));
    // Allocate memory for the log
    char *log = (char *) malloc(log_size);
    // Get the log
    CheckError( clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, log_size, log, NULL));

    // Print the log
    printf("%s\n", log);
  #endif


  CheckError(ret);
  /* Create OpenCL Kernel */
  kernel = clCreateKernel(program, "compute_symbol_composition", &ret);
  CheckError(ret);
  // Copy results from the memory buffer
  //ret = clEnqueueReadBuffer(command_queue, memobj, CL_TRUE, 0, MEM_SIZE * sizeof(char),string, 0, NULL, NULL);


  // DONE OPENCL ENVIRONMENT SETUP
 
  size_t global_item_size = 4;
  size_t local_item_size = 1;
  int total_elements = 0;
  int block_size = 32;

  std::map< int,std::map<int,int> > map_data;
  map_data[0][0] = 1;


  //This structure will be used to make sure the correct source states get visited 
  std::stack< std::tuple<int,int> > stack_sources;
  composed_fst_node* states;
  states = (composed_fst_node*)malloc( m2.num_states * m.num_states *sizeof(composed_fst_node));

  //The number of outputs for the first transducer and inputs for the second one must match
  int states_width = std::max(m.num_outputs, m2.num_inputs);

  int num_elements1;
  int num_elements2;
  int start_index1;
  int start_index2;

  int write_start_index = 0;
  size_t cl_DimBlock[2], cl_DimGrid[2];
  //compute compositions from the start state
  for(int offset_vocab = 0; offset_vocab <= m.num_outputs; offset_vocab++){
    start_index1 = m.output_symbol_offsets[offset_vocab];
    start_index2 = m2.input_symbol_offsets[offset_vocab];

    num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
    num_elements2 = m2.input_symbol_offsets[offset_vocab+1] - start_index2;
    total_elements+=num_elements1*num_elements2;

    if(num_elements1 > 0 && num_elements2 > 0){

      // Set OpenCL Kernel Parameters
      CheckError(clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&m_to_states));
      CheckError(clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&m_input));
      CheckError(clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&m_prob));
      CheckError(clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&m2_to_states));
      CheckError(clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&m2_output));
      CheckError(clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&m2_prob));

      CheckError(clSetKernelArg(kernel, 6, sizeof(int), (void *)&start_index1));
      CheckError(clSetKernelArg(kernel, 7, sizeof(int), (void *)&num_elements1));
      CheckError(clSetKernelArg(kernel, 8, sizeof(int), (void *)&start_index2));
      CheckError(clSetKernelArg(kernel, 9, sizeof(int), (void *)&num_elements2));

      CheckError(clSetKernelArg(kernel, 10, sizeof(cl_mem), (void *)&gpu_states));

      CheckError(clSetKernelArg(kernel, 11, sizeof(int), (void *)&num_elements2));
      CheckError(clSetKernelArg(kernel, 12, sizeof(int), (void *)&write_start_index));
/*
      //Call OpenCL Kernel
      cl_DimBlock[0] = //ceildiv(num_elements1, 32);//num_elements1;
      cl_DimBlock[1] = //ceildiv(num_elements2, 32);//num_elements2;

      cl_DimGrid[0] = num_elements1;//ceildiv(num_elements1, 32);
      cl_DimGrid[1] = num_elements2;///ceildiv(num_elements2, 32);
*/

      cl_DimBlock[0] = 32;//ceildiv(num_elements1, 32);//num_elements1;
      cl_DimBlock[1] = 32;//ceildiv(num_elements2, 32);//num_elements2;

      cl_DimGrid[0] = roundUp(num_elements1,32);//ceildiv(num_elements1, 32);
      cl_DimGrid[1] = roundUp(num_elements2,32);///ceildiv(num_elements2, 32);
 
      CheckError( clEnqueueNDRangeKernel(command_queue,kernel,2,NULL,cl_DimGrid, cl_DimBlock/*&global_item_size,&local_item_size*/,0,NULL,NULL));
      CheckError(ret); 
      //printf("elements %d - %d \n",num_elements1,num_elements2);
      write_start_index+= num_elements1 * num_elements2;
    }
  
  }
 
  if(write_start_index > 0){
    // Copy results from the memory buffer
    CheckError( clEnqueueReadBuffer(command_queue, gpu_states, CL_TRUE, 0, write_start_index * sizeof(composed_fst_node),states, 0, NULL, NULL));
  }

   CheckError(ret);
  // Check the target states
  int target1,target2;
  int counter = 0;
  for(int i=0;i< write_start_index;i++){
    target1 = states[i].to_state1;
    target2 = states[i].to_state2;
    int threadIdx = states[i].output;
    int threadIdy = states[i].input;
    if(target1 < m.num_states && target2 <m2.num_states && target1 >0  && target2 > 0)
    {
      if(map_data[target1][target2] == 0){
         printf("(%d,%d)\n",target1,target2);
        stack_sources.push(std::tie(target1, target2));
        map_data[target1][target2] = 1;
        counter++;
      }
    }
  }


  // compute the transitions for the other states (in this case we will try to compose all states we can)
  do{
    int start1 = std::get<0>(stack_sources.top());
    int start2 = std::get<1>(stack_sources.top());
    stack_sources.pop();

    write_start_index = 0;
    total_elements = 0;

    //printf("start %d and %d \n",start1,start2);
    int offset_start = start1 * (m.num_outputs + 1);
    int offset_start2 = start2 * (m2.num_inputs + 1);

    int offset_vocab;
    int offset_vocab2;
    //printf("ENTER LOOP %d - %d\n",start1,start2);
    for(int token = 0;token<m.num_outputs; token++){
      offset_vocab = offset_start + token;
      offset_vocab2 = offset_start2 + token;

      //printf("Delete 2 %d - %d \n",offset_vocab,offset_vocab2);
      //Compute the offsets for the different states
      start_index1 = m.output_symbol_offsets[offset_vocab];
      start_index2 = m2.input_symbol_offsets[offset_vocab2];
      //printf("Delete 3\n");
      num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
      num_elements2 = m2.input_symbol_offsets[offset_vocab2+1] - start_index2;
      total_elements+=num_elements1*num_elements2;
 
      //printf("Delete 1\n");
      if(num_elements1 > 0 && num_elements2 > 0){
        total_elements+=num_elements1*num_elements2;


        // Set OpenCL Kernel Parameters
        CheckError(clSetKernelArg(kernel, 0, sizeof(cl_mem), (void *)&m_to_states));
        CheckError( clSetKernelArg(kernel, 1, sizeof(cl_mem), (void *)&m_input));
        CheckError( clSetKernelArg(kernel, 2, sizeof(cl_mem), (void *)&m_prob));
        CheckError( clSetKernelArg(kernel, 3, sizeof(cl_mem), (void *)&m2_to_states));
        CheckError( clSetKernelArg(kernel, 4, sizeof(cl_mem), (void *)&m2_output));
        CheckError( clSetKernelArg(kernel, 5, sizeof(cl_mem), (void *)&m2_prob));

        CheckError( clSetKernelArg(kernel, 6, sizeof(int), (void *)&start_index1));
        CheckError( clSetKernelArg(kernel, 7, sizeof(int), (void *)&num_elements1));
        CheckError( clSetKernelArg(kernel, 8, sizeof(int), (void *)&start_index2));
        CheckError( clSetKernelArg(kernel, 9, sizeof(int), (void *)&num_elements2));

        CheckError( clSetKernelArg(kernel, 10, sizeof(cl_mem), (void *)&gpu_states));

        CheckError( clSetKernelArg(kernel, 11, sizeof(int), (void *)&num_elements2));
        CheckError( clSetKernelArg(kernel, 12, sizeof(int), (void *)&write_start_index));

      cl_DimBlock[0] = 32;//ceildiv(num_elements1, 32);//num_elements1;
      cl_DimBlock[1] = 32;//ceildiv(num_elements2, 32);//num_elements2;

      cl_DimGrid[0] = roundUp(num_elements1,32);//ceildiv(num_elements1, 32);
      cl_DimGrid[1] = roundUp(num_elements2,32);///ceildiv(num_elements2, 32);

      //Global and local
      CheckError(clEnqueueNDRangeKernel(command_queue,kernel,2,NULL,cl_DimGrid, cl_DimBlock/*&global_item_size,&local_item_size*/,0,NULL,NULL));
      CheckError(ret);


        write_start_index+= num_elements1 * num_elements2;
      }
    }

    //Do a mem copy with the composed elements
    CheckError( clEnqueueReadBuffer(command_queue, gpu_states, CL_TRUE, 0, write_start_index * sizeof(composed_fst_node),states, 0, NULL, NULL));

    // Check the target states
    for(int i=0;i< write_start_index;i++){
      target1 = states[i].to_state1;
      target2 = states[i].to_state2;
      //printf("Target 1 is: %d and target 2: %d \n",target1,target2);
      if(target1 < m.num_states && target2 <m2.num_states && target1 >0  && target2 > 0)
      {
        if(map_data[target1][target2] == 0){
          printf("(%d,%d)\n",target1,target2);
          stack_sources.push(std::tie(target1, target2));

          map_data[target1][target2] = 1;
        }
      }
    }
  }
  while(!stack_sources.empty());



  // Finalization
  CheckError(clFlush(command_queue));
  CheckError(clFinish(command_queue));
  CheckError(clReleaseKernel(kernel));
  CheckError(clReleaseProgram(program));
  CheckError(clReleaseMemObject(m_to_states));
  CheckError(clReleaseMemObject(m_input));
  CheckError(clReleaseMemObject(m_prob));
  CheckError(clReleaseMemObject(m2_to_states));
  CheckError(clReleaseMemObject(m2_output));
  CheckError(clReleaseMemObject(m2_prob));
  CheckError(clReleaseMemObject(gpu_states));

  CheckError(clFinish(command_queue));
  CheckError(clReleaseCommandQueue(command_queue));
  CheckError(clReleaseContext(context));

  free(source_str);
}

