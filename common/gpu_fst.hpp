#ifndef GPU_FST_HPP
#define GPU_FST_HPP

#include <thrust/device_vector.h>
#include <thrust/copy.h>
#include <thrust/fill.h>
#include <vector>
#include <iostream>
#include <cfloat>
#include "fst.hpp"
#include "gpu_utils.hpp"
#include "numberizer.hpp"


struct input_fst{
  state_t initial;
  thrust::host_vector<int> source_states;

  thrust::host_vector<int> output_symbol_offsets;
  std::map< int,std::map<int,int> > output_symbol_offset_map;
  
  thrust::device_vector<state_t> target_states;
  thrust::device_vector<sym_t> inputs;
  thrust::device_vector<prob_t> probs;

  thrust::device_vector<state_t> final_states;
  thrust::device_vector<prob_t> final_probs;

  state_t num_states;
  sym_t num_inputs, num_outputs;

  //constructor for the fst representation
  input_fst(fst &&m)
    :
    initial(m.initial),
    num_states(m.num_states),
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    source_states(m.num_states+1),
    output_symbol_offsets((m.num_outputs+1) * m.num_states),
    target_states(m.transitions.size()),
    inputs(m.transitions.size()),
    probs(m.transitions.size()),
    final_states(m.finals.size()),
    final_probs(m.finals.size())
  {
    //Sort edge transitions accordingly
    sort_by_fromstate_output(m);

    // Begin filling data structure
    sym_t e_last = -1;
    int q_last = 0;
    int compute_index = 0;
    
    int last_index = 0; //used for the map
    //int last_symbol = 0;

    for (int i=0; i<m.transitions.size(); i++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];

      //Keep track of the current state read
      if(q > q_last) {
        for(int x = e_last+1; x < m.num_outputs+1; x++){
          compute_index = q_last * (m.num_outputs+1) + x;
          output_symbol_offsets[compute_index] = i;
          //printf("offsets[%d][%d]=%d \n",q_last,e_last,i);


/*
          if(x == m.num_outputs){
            printf("offsets[%d]=%d- \n",compute_index,i);

            output_symbol_offset_map[q_last][e_last-1] = last_index;
            output_symbol_offset_map[q_last][e_last] = i;
            printf("map[%d]=%d- \n",compute_index-1,last_index);
            printf("map[%d]=%d- \n",compute_index,i);
          }
*/
        }
        q_last = q;
        e_last = -1;
      }

      char flag = 1;
      //Keep track of the output labels that are read
      while(e > e_last){
        if(flag){
          output_symbol_offset_map[q_last][e_last] = last_index;
          output_symbol_offset_map[q_last][e_last+1] = i;
          //printf("map[%d][%d]=%d \n",q_last,e_last,last_index);
          //printf("map[%d][%d]=%d \n",q_last,e_last+1,i);
          last_index = i;
          flag = 0;
        }
        e_last++;
        compute_index = q_last * (m.num_outputs+1) + e_last;

        output_symbol_offsets[compute_index] = i;
        //printf("offsets[%d][%d]=%d \n",q_last,e_last,i);
      }
    }

    for(int i= (q_last * (m.num_outputs+1) + e_last)+1; i< (m.num_outputs+1) * m.num_states; i++)
    {
      output_symbol_offsets[i] = m.transitions.size();
    }

    //output_symbol_offsets_device = output_symbol_offsets;
    //Offload data into the GPU
    unzip_to_device<1>(m.transitions, target_states);
    unzip_to_device<2>(m.transitions, inputs);
    unzip_to_device<4>(m.transitions, probs);

    unzip_to_device<0>(m.finals, final_states);
    unzip_to_device<1>(m.finals, final_probs);
  }
};

struct input_fst_linear{
  int transitions;
  std::vector<int> source_states;
  std::vector<int> target_states;
  std::vector<int> input_symbols;
  std::vector<int> output_symbols;
  std::vector<float> probs;

  //constructor for the fst representation
  input_fst_linear(fst &&m)
    :
    transitions(m.transitions.size()),
    source_states(m.transitions.size()),
    target_states(m.transitions.size()),
    input_symbols(m.transitions.size()),
    output_symbols(m.transitions.size()),
    probs(m.transitions.size())
  {
    printf("Allocated memory \n");
    //Sort edge transitions accordingly
    sort_by_fromstate_output(m);

    for (int i=0; i<m.transitions.size(); i++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];

      source_states[i] = q;
      target_states[i] = r;
      input_symbols[i] = f;
      output_symbols[i] = e;
      probs[i] = p;
    }
  }
};





struct output_fst{
  state_t initial;
  thrust::host_vector<int> source_states;

  thrust::host_vector<int> input_symbol_offsets;
  std::map< int,std::map<int,int> > input_symbol_offset_map;
 
  thrust::device_vector<state_t> target_states;
  thrust::device_vector<sym_t> inputs;
  thrust::device_vector<prob_t> probs;

  thrust::device_vector<state_t> final_states;
  thrust::device_vector<prob_t> final_probs;

  state_t num_states;
  sym_t num_inputs, num_outputs;

  //constructor for the fst representation
  output_fst(fst &&m)
    :
    initial(m.initial),
    num_states(m.num_states),
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    source_states(m.num_states+1),
    input_symbol_offsets((m.num_inputs+1) * m.num_states),
    target_states(m.transitions.size()),
    inputs(m.transitions.size()),
    probs(m.transitions.size()),
    final_states(m.finals.size()),
    final_probs(m.finals.size())
  {
    //Sort edge transitions accordingly
    sort_by_fromstate_input(m);

    // Begin filling data structure
    sym_t f_last = -1;
    int q_last = 0;
    int compute_index = 0;
    
    int last_index = 0;

    for (int i=0; i<m.transitions.size(); i++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];
      //printf("Transition %d %d %d %d \n",q,r,f,e);

      //Keep track of the current state read
      if(q > q_last) {
        for(int x = f_last+1; x < m.num_inputs+1; x++){
          compute_index = q_last * (m.num_inputs+1) + x;
          input_symbol_offsets[compute_index] = i;
        }
        q_last = q;
        f_last = -1;
      }

      char flag = 1;
      //Keep track of the output labels that are read
      while(f > f_last){
        if(flag){
          input_symbol_offset_map[q_last][f_last] = last_index;
          input_symbol_offset_map[q_last][f_last+1] = i;
          //printf("map[%d][%d]=%d \n",q_last,f_last,last_index);
          //printf("map+1[%d][%d]=%d \n",q_last,f_last+1,i);
          last_index = i;
          flag = 0;
        }

        f_last++;
        compute_index = q_last * (m.num_inputs+1) + f_last;
        input_symbol_offsets[compute_index] = i;
        //printf("offsets[%d][%d]=%d \n",q_last,f_last,i);
/*
        if(f == f_last){
          input_symbol_offset_map[q_last][f_last] = last_index;
          input_symbol_offset_map[q_last][f_last] = i;
          last_index = i;
        }
*/
      }
    }

    for(int i= (q_last * (m.num_inputs+1) + f_last)+1; i< (m.num_inputs+1) * m.num_states; i++)
    {
      input_symbol_offsets[i] = int(m.transitions.size());
    } 
 
    //input_symbol_offsets_device = input_symbol_offsets;
    //Offload data into the GPU

       cudaDeviceSynchronize();

    unzip_to_device<1>(m.transitions, target_states);
    unzip_to_device<3>(m.transitions, inputs);
    unzip_to_device<4>(m.transitions, probs);

    unzip_to_device<0>(m.finals, final_states);
    unzip_to_device<1>(m.finals, final_probs);

       cudaDeviceSynchronize();
  }
};



struct compact_fst_node{
  //unsigned int id;
  short from_state;
  short to_state;
  short output;
  short input;
  float prob;

  __device__ compact_fst_node(){
    from_state = 0;
    to_state = 0;
    output = 0;
    input = 0;
    prob = 0;
  }
  //__device__ __host__ bool operator<(const compact_fst_node& b) const { id < b.id; }
};


//TODO: Implement function to free these pointers from memory
struct fst_and_keys{
  thrust::device_vector<compact_fst_node> fst_edges;
  thrust::device_vector<unsigned long long> fst_edge_id;
  thrust::device_vector<compact_fst_node> fst_edges_aux;
  thrust::device_vector<unsigned long long> fst_edge_id_aux;

  fst_and_keys(int size_alloc)
    :
    fst_edges(size_alloc),
    fst_edge_id(size_alloc),
    fst_edges_aux(size_alloc),
    fst_edge_id_aux(size_alloc)
  {

/*
    cudaMalloc(&fst_edges, sizeof(compact_fst_node) * size_alloc);  
    cudaMalloc(&fst_edge_id, sizeof(unsigned int) * size_alloc);
    cudaMalloc(&fst_edges_aux, sizeof(compact_fst_node) * size_alloc);
    cudaMalloc(&fst_edge_id_aux, sizeof(unsigned int) * size_alloc);
*/
  }

  void free_structure(){

/*
    thrust::device_delete(fst_edges.data());
    thrust::device_delete(fst_edge_id.data());
    thrust::device_delete(fst_edges_aux.data());
    thrust::device_delete(fst_edge_id_aux.data());

    cudaFree(fst_edges);
    cudaFree(fst_edge_id);
    cudaFree(fst_edges_aux);
    cudaFree(fst_edge_id_aux);
*/
  }
};




struct compact_node_equality
{
  __host__ __device__
  bool operator()(const unsigned long long &a, const unsigned long long &b){
    return (a == b);
  }
};



struct reduce_compact_fst_node
{
  __host__ __device__
  compact_fst_node operator()(const compact_fst_node &a, const compact_fst_node &b){
    compact_fst_node t = a;
    t.prob += b.prob;
    return t;
  }
};



struct sort_pointers{
  compact_fst_node* d_out;      
  unsigned int block_sz;// = MAX_BLOCK_SZ;
  unsigned int max_elems_per_block;// = block_sz;
  unsigned int grid_sz;// = num_elems / max_elems_per_block;
  unsigned int s_data_len;// = max_elems_per_block;
  unsigned int s_mask_out_len;// = max_elems_per_block + (max_elems_per_block / NUM_BANKS);
  unsigned int s_merged_scan_mask_out_len; // max_elems_per_block;
  unsigned int s_mask_out_sums_len; //= 4; // 4-way split
  unsigned int s_scan_mask_out_sums_len; //= 4;
  unsigned int shmem_sz;// = (s_data_len * sizeof(compact_fst_node)
                           // + s_mask_out_len * sizeof(unsigned int)
                           // + s_merged_scan_mask_out_len * sizeof(unsigned int)
                            //+ s_mask_out_sums_len * sizeof(unsigned int)
                            //+ s_scan_mask_out_sums_len * sizeof(unsigned int));
  unsigned int* d_prefix_sums;
  unsigned int* d_block_sums;
  unsigned int* d_scan_block_sums;
  unsigned int d_prefix_sums_len;
  unsigned int d_block_sums_len;

  sort_pointers(unsigned int num_elems){
    cudaMalloc(&d_out, sizeof(compact_fst_node) * num_elems);
    block_sz = 1024;
    max_elems_per_block = block_sz;
    grid_sz = num_elems / max_elems_per_block;
    if (num_elems % max_elems_per_block != 0){
      grid_sz += 1;
    }
    d_prefix_sums_len = num_elems;
    d_block_sums_len = 4 * grid_sz;

    cudaMalloc(&d_prefix_sums, sizeof(unsigned int) * d_prefix_sums_len);
    cudaMalloc(&d_block_sums, sizeof(unsigned int) * d_block_sums_len);
    cudaMalloc(&d_scan_block_sums, sizeof(unsigned int) * d_block_sums_len);

    s_data_len = max_elems_per_block;
    s_mask_out_len = max_elems_per_block + (max_elems_per_block / 32);
    s_merged_scan_mask_out_len = max_elems_per_block;
    s_mask_out_sums_len = 4; // 4-way split
    s_scan_mask_out_sums_len = 4;
    shmem_sz = (s_data_len * sizeof(compact_fst_node)
                            + s_mask_out_len * sizeof(unsigned int)
                            + s_merged_scan_mask_out_len * sizeof(unsigned int)
                            + s_mask_out_sums_len * sizeof(unsigned int)
                            + s_scan_mask_out_sums_len * sizeof(unsigned int));    

  }

  void zero_out_counts(){
    cudaMemset(d_prefix_sums, 0, sizeof(unsigned int) * d_prefix_sums_len);
    cudaMemset(d_block_sums, 0, sizeof(unsigned int) * d_block_sums_len);
    cudaMemset(d_scan_block_sums, 0, sizeof(unsigned int) * d_block_sums_len);
  }


  void free_pointers(){
    cudaFree(d_out);
    cudaFree(d_prefix_sums);
    cudaFree(d_block_sums);
    cudaFree(d_scan_block_sums);
  }


};









struct compact_node_sort
{
  __host__ __device__
  bool operator()(compact_fst_node &a,compact_fst_node &b){
    return (a.to_state <= b.to_state && a.input <= b.input && a.output <= b.output);
  }
};




struct composable_gpu_input {
  state_t initial;
   std::vector<int> output_offsets;

  thrust::device_vector<int> device_from_states;
  thrust::device_vector<state_t> from_states;
  thrust::device_vector<state_t> to_states;

  thrust::device_vector<sym_t> outputs; 
  thrust::device_vector<sym_t> inputs;

  thrust::device_vector<prob_t> probs;

  thrust::device_vector<state_t> final_states;
  thrust::device_vector<prob_t> final_probs;

  state_t num_states;
  sym_t num_inputs, num_outputs;

  composable_gpu_input(fst &&m)
    :
    initial(m.initial),
    num_states(m.num_states), 
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    output_offsets(m.num_outputs+1),
    device_from_states(m.num_states+1),
    from_states(m.transitions.size()),
    to_states(m.transitions.size()),
    outputs(m.transitions.size()),
    inputs(m.transitions.size()),
    probs(m.transitions.size()),
    final_states(m.finals.size()),
    final_probs(m.finals.size())
  {
    int verbose = 1;

    if (verbose) std::cerr << "sorting transitions...";
    //sort_by_output_fromstate_tostate(m);
    sort_by_output_fromstate_tostate(m);
    if (verbose) std::cerr << "done.\ncomputing input offsets...";

    sym_t q_last = -1;
    int i_last = 0;
    for (int i=0; i<m.transitions.size(); i++) {
      //printf("ITERATION\n");
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];
   
      while (e > q_last) {
	q_last++;

	output_offsets[q_last] = i;

#if 1
        if(q_last >0){
         if(output_offsets[q_last]-output_offsets[q_last-1] < 0){
            printf("The input offset is %d and val %d - %d and index %d \n",output_offsets[q_last]-output_offsets[q_last-1],output_offsets[q_last],output_offsets[q_last-1],q_last);
 	    exit(0);
          }
        }
//printf("-%d and %d\n",q_last,num_outputs);
#endif

        i_last = i;
        if(i == m.num_outputs){
          q_last++;
          output_offsets[q_last] = i_last+1;
        }
      }
      //outputs[i] = e;
    }

    while(q_last < m.num_outputs){
      q_last++;
      output_offsets[q_last] = i_last;
    }
    output_offsets.back() = m.transitions.size();

    device_from_states = output_offsets;
    if (verbose) std::cerr << "done.\ncopying to device...";
    unzip_to_device<0>(m.transitions, from_states);
    unzip_to_device<1>(m.transitions, to_states);
    unzip_to_device<2>(m.transitions, inputs);
    unzip_to_device<3>(m.transitions, outputs);
    unzip_to_device<4>(m.transitions, probs);

    unzip_to_device<0>(m.finals, final_states);
    unzip_to_device<1>(m.finals, final_probs);

    if (verbose) std::cerr << "done.\n";
  }
};



struct composable_gpu_output{
  state_t initial;
  std::vector<int> input_offsets;

  thrust::device_vector<int> device_from_states;
  thrust::device_vector<state_t> from_states;
  thrust::device_vector<state_t> to_states;

  thrust::device_vector<sym_t> outputs;
  thrust::device_vector<sym_t> inputs;

  thrust::device_vector<prob_t> probs;

  thrust::device_vector<state_t> final_states;
  thrust::device_vector<prob_t> final_probs;

  state_t num_states;
  sym_t num_inputs, num_outputs;

  composable_gpu_output(fst &&m)
    :
    initial(m.initial),
    num_states(m.num_states), 
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    input_offsets(m.num_inputs+1),
    device_from_states(m.num_states+1),
    from_states(m.transitions.size()),
    to_states(m.transitions.size()),
    outputs(m.transitions.size()),
    inputs(m.transitions.size()),
    probs(m.transitions.size()),
    final_states(m.finals.size()),
    final_probs(m.finals.size())
  {
    int verbose = 1;

    if (verbose) std::cerr << "sorting transitions...";
    sort_by_input_fromstate_tostate(m);
    if (verbose) std::cerr << "done.\ncomputing input offsets...";

    sym_t q_last = -1;
    int i_last = 0;
    for (int i=0; i<m.transitions.size(); i++) {
      //printf("ITERATION\n");
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];
      //printf("[%d %d %d %d %f]\n",q,r,f,e,p);
      while (f > q_last) {
	q_last++;

	input_offsets[q_last] = i;


#if 1
        if(q_last >0){
          if(input_offsets[q_last]-input_offsets[q_last-1] < 0){
            printf("The output offset is %d and val %d - %d and index: %d \n",input_offsets[q_last]-input_offsets[q_last-1],input_offsets[q_last],input_offsets[q_last-1],q_last);
            //TODO: ADD
            exit(0);
          }
        }
#endif


        i_last = i;
        //printf("Front offset [%d] is %d \n",q_last,i);
        if(i == num_inputs){
          q_last++;
          input_offsets[q_last] = i_last+1;
        }
      }
      //outputs[i] = e;
    }

    while(q_last < num_inputs-1){
      q_last++;
      input_offsets[q_last] = i_last;
    }
    input_offsets.back() = m.transitions.size();

    device_from_states = input_offsets;
    if (verbose) std::cerr << "done.\ncopying to device...";
    unzip_to_device<0>(m.transitions, from_states);
    unzip_to_device<1>(m.transitions, to_states);
    unzip_to_device<2>(m.transitions, inputs);
    unzip_to_device<3>(m.transitions, outputs);
    unzip_to_device<4>(m.transitions, probs);

    unzip_to_device<0>(m.finals, final_states);
    unzip_to_device<1>(m.finals, final_probs);

    if (verbose) std::cerr << "done.\n";
  }
};


struct composed_fst_node{
  int from_state1;
  int to_state1;
 
  int from_state2;
  int to_state2;
  int output;
  int input;

  float prob;

  composed_fst_node():
  from_state1(),
  from_state2(),
  to_state1(),
  to_state2(),
  output(),
  input(),
  prob(-FLT_MAX)
  {
  }
 
  __device__ composed_fst_node(int s):
  //from_state1(),
  //from_state2(),
  to_state1(),
  to_state2(),
  output(),
  input(),
  prob(-FLT_MAX)
  {
  }
 
  composed_fst_node(int from,int to, int out, int in, float p):
  //from_state1(from),
  //from_state2(from),
  to_state1(to),
  to_state2(to),
  output(out),
  input(in),
  prob(p)
  {}
};

__host__ __device__ bool operator<(const composed_fst_node &in1, const composed_fst_node &in2) { return (in1.to_state1 < in2.to_state1 && in1.to_state2 < in2.to_state2 && in1.output < in2.output && in1.input < in2.input); }

struct composed_fst_node_noProb{
  int from_state1;
  int to_state1;
 
  int from_state2;
  int to_state2;
  int output;
  int input;


  composed_fst_node_noProb():
  from_state1(),
  from_state2(),
  to_state1(),
  to_state2(),
  output(),
  input()
  {
  }
 
  __device__ composed_fst_node_noProb(int s):
  //from_state1(),
  //from_state2(),
  to_state1(),
  to_state2(),
  output(),
  input()
  {
  }
 
  composed_fst_node_noProb(int from,int to, int out, int in):
  //from_state1(from),
  //from_state2(from),
  to_state1(to),
  to_state2(to),
  output(out),
  input(in)
  {}
};

//bool operator< ( composed_fst_node a1, composed_fst_node b1 ) { return (a1.from_state1+a1.to_state2+a1.input+a1.output) < (b1.from_state1+b1.to_state2+b1.input+b1.output) ; }

struct composed_fst{
  int num_nodes;
  composed_fst_node reset_state;
  thrust::host_vector<composed_fst_node> states;
  thrust::device_vector<composed_fst_node> gpu_states;

  composed_fst(int in_nodes):
  num_nodes(in_nodes),
  states(in_nodes)
  {
    //states = (composed_fst_node *) malloc( in_nodes * sizeof(composed_fst_node) );

    for(int foo=0;foo<in_nodes;foo++){
      //states[foo].from_state1 = 0;
      //states[foo].from_state2 = 0;

      states[foo].to_state1 = 0;
      states[foo].to_state2 = 0;
      states[foo].output = 0;
      states[foo].input = 0;

      states[foo].prob = -FLT_MAX;

    }

    // Reset state
    //reset_state.from_state1 = 0;
    //reset_state.from_state2 = 0;
    reset_state.to_state1 = 0;
    reset_state.to_state2 = 0;
    reset_state.output = 0;
    reset_state.input = 0;
    reset_state.prob = -FLT_MAX;

    //cudaMalloc((void **)&gpu_states, in_nodes * sizeof(composed_fst_node));

    cudaError_t error3 = cudaGetLastError();
    if(error3 != cudaSuccess)
    {
      // print the CUDA error message and exit
      printf("CUDA error main() 4----: %s\n", cudaGetErrorString(error3));
      exit(-1);
    }

    //cudaMemcpy(gpu_states, states, in_nodes * sizeof(composed_fst_node), cudaMemcpyHostToDevice);
    gpu_states = states;
    states.clear();
    states.shrink_to_fit();
  }

  void realloc_gpu_states(int num_elements){
    //gpu_states.resize(num_elements,reset_state);
  }
  
};

void copy_fst_back(composed_fst &fst_copy, int num_nodes){
  //printf("Copying %d nodes and size %d\n",num_nodes,int(sizeof(composed_fst_node)));
  fst_copy.states = fst_copy.gpu_states;
  //udaMemcpy(fst_copy.states, fst_copy.gpu_states, num_nodes * sizeof(composed_fst_node), cudaMemcpyDeviceToHost);
  //cudaMemcpy(&fst_copy.num_nodes, fst_copy.size, sizeof(int), cudaMemcpyDeviceToHost);
}

void print_fst(composed_fst &test, composable_gpu_input &m,composable_gpu_output &m2,int num_words,const numberizer &in , const numberizer &out){
  int REG_PRINT = 1;
  for(int foo=0;foo<num_words;foo++){
    //int from_st_normal = test.states[foo].from_state1;
    int to_st_normal = test.states[foo].to_state1;
    //int from_st_normal2 = test.states[foo].from_state2;
    int to_st_normal2 = test.states[foo].to_state2;
    //int from_st_x = from_st_normal / m2.num_states; 
    //int from_st_y = from_st_normal %  m.num_states;
    int to_st_x = to_st_normal / m2.num_states;
    int to_st_y = to_st_normal % m.num_states;

    float the_prob = test.states[foo].prob;
    if(REG_PRINT){
      if(the_prob > -FLT_MAX){
        std::string in_label = in.num_to_word(test.states[foo].input);
        std::string out_label = out.num_to_word(test.states[foo].output);
        printf("From %d to %d and prob %f in: %s out: %s index: %d\n",to_st_normal2,to_st_normal,exp(the_prob),in_label.c_str(),out_label.c_str(),foo);  
      }
    }
    else{
      if(the_prob > -FLT_MAX){
        printf("The input is %d \n",test.states[foo].input);
        std::string in_label = in.num_to_word(test.states[foo].input);
        std::string out_label = out.num_to_word(test.states[foo].output);
        printf("From (%d,%d) to (%d,%d) and prob %f in: %s out: %s \n",to_st_normal,to_st_normal,to_st_x,to_st_y,exp(the_prob),in_label.c_str(),out_label.c_str());
      }
    }
  }
}

//TODO: This method might not be adding the correct transitions
fst read_composed_fst(composed_fst &test, int num_inputs, int num_outputs, int size) {
  using namespace std;
  fst m;

  m.num_inputs = num_inputs;
  m.num_outputs = num_outputs;

  printf("Read Input Composed FST %d \n",test.num_nodes);
 
  for(int foo = 0;foo < size; foo++){
      //int from_st_normal = test.states[foo].from_state1;
      int to_st_normal = test.states[foo].to_state1;
      int in_label = test.states[foo].input;
      int out_label = test.states[foo].output;
      float the_prob = test.states[foo].prob;

    if(the_prob < 0){
      //printf("Adding: %d %d %d %d %f \n",from_st_normal,to_st_normal,in_label,out_label,exp(the_prob));
      m.add_transition(to_st_normal,to_st_normal,in_label,out_label,the_prob);
    }
  }
  return m;
}

int get_compositon_offset_size(composable_gpu_input &m, composable_gpu_output &m2){
  int out_labels_fst1 = m.output_offsets.size() -1;
  int in_labels_fst2 = m2.input_offsets.size() -1;

  int total_size = 0;
  for(int out_labels = 0; out_labels < out_labels_fst1 && out_labels < in_labels_fst2; out_labels++){
    int offset_size = m.output_offsets[out_labels+1] - m.output_offsets[out_labels];
    int second_offset_size = m2.input_offsets[out_labels+1] - m2.input_offsets[out_labels];

    if(offset_size < 0 || second_offset_size <0){
     printf("Sizes are %d and %d index %d and max %d * %d\n",offset_size,second_offset_size,out_labels,out_labels_fst1,in_labels_fst2);
     printf("FST1 %d - %d\n",m.output_offsets[out_labels+1], m.output_offsets[out_labels]);
     printf("FST2 %d - %d\n",m2.input_offsets[out_labels+1],m2.input_offsets[out_labels]);
     //TODO: add
     exit(0);
    }

    if(offset_size > 0 && second_offset_size > 0){
      int add_term = (offset_size * second_offset_size);
      total_size += add_term;
      //printf("Adding: %d \n",add_term);
    }
  }
  return total_size;
}

#endif

