struct composed_fst_node{
  int to_state1;
  int to_state2;
  int output;
  int input;
  float prob;
};

__kernel void compute_symbol_composition(
  __global int* m_to_states,
  __global int* m_input,
  __global float* m_prob,
  __global int* m2_to_states,
  __global int* m2_output,
  __global float* m2_prob,
 int offset_start,
 int offset_size,
 int offset_start2,
 int offset_size2,
  __global struct composed_fst_node *gpu_states,
  int write_width,
  int write_start)
{
  int idx = get_global_id(0); 
  int idy = get_global_id(1);

  int index_x;
  int index_y;


  struct composed_fst_node thread_node;

  int to_state_x;
  int to_state_y;
  int write_index;
  if(idx < offset_size && idy < offset_size2){

    index_x = offset_start + idx;
    index_y = offset_start2 + idy;

    to_state_x = m_to_states[index_x];
    to_state_y =  m2_to_states[index_y];
    thread_node.to_state1 = to_state_x;
    thread_node.to_state2 = to_state_y;
    thread_node.output = m2_output[index_y];
    thread_node.input = m_input[index_x];
    thread_node.prob = m_prob[index_x] + m2_prob[index_y];

    write_index = write_start + (idx * write_width + idy);
    gpu_states[write_index] = thread_node;
  }
}
