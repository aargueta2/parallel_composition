/**
  Author: Arturo Argueta
  This file contains the main methods to do composition on a CPU (No multi-core)
**/
#include <tuple>
#include <stack>
#include <iostream>
#include<set>
#include<tuple>
#include <list>
#include <vector>

int ceildiv(int x, int y) { return (x-1)/y+1; }

int composed_index(int index_1,int index_2,int num_states){return index_1*num_states+index_2;}

int roundUp(int numToRound, int multiple)
{
    if (multiple == 0)
        return numToRound;

    int remainder = numToRound % multiple;
    if (remainder == 0)
        return numToRound;

    return numToRound + multiple - remainder;
}


unsigned long long int compute_efficient_composition(input_fst &m, output_fst &m2, compact_fst_node* states){

  std::set<std::tuple<int,int> > setOfStates;
  //Map to keep track of the visited states during composition 
  std::map< int,std::map<int,int> > map_data;
  map_data[0][0] = 0;

  //This structure will be used to make sure the correct source states get visited 
  std::queue< std::tuple<int,int> > stack_sources;

  //The number of outputs for the first transducer and inputs for the second one must match
  int states_width = std::max(m.num_outputs, m2.num_inputs);
  unsigned long long int total_elements = 0;
  unsigned int total_states = 1;

  int num_elements1;
  int num_elements2;
  int start_index1;
  int start_index2;

  unsigned long long int write_start_index = 0;
  compact_fst_node thread_node = compact_fst_node();
  unsigned long long int write_index;
  //compute states leaving state 0 from both transducers
  for(int offset_vocab = 0; offset_vocab < m.num_outputs; offset_vocab++){

    start_index1 = m.output_symbol_offsets[offset_vocab];
    start_index2 = m2.input_symbol_offsets[offset_vocab];

    num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
    //This is the width
    num_elements2 = m2.input_symbol_offsets[offset_vocab+1] - start_index2;

    total_elements+=num_elements1*num_elements2;
    if(num_elements1 > 0 && num_elements2 > 0){
      for(int x = 0; x < num_elements1; x++){
        for(int y=0; y< num_elements2; y++){
          thread_node.from_state = 0; //*m2.num_states + 0;//0;
        
          if(map_data[m.target_states[start_index1 + x]][m2.target_states[start_index2 + y]] == 0){
            map_data[m.target_states[start_index1 + x]][m2.target_states[start_index2 + y]] = total_states;
            total_states++;
            stack_sources.push(std::tie(m.target_states[start_index1 + x], m2.target_states[start_index2 + y]));
          }

          thread_node.to_state = map_data[m.target_states[start_index1 + x]][m2.target_states[start_index2 + y]];
          thread_node.output = m2.inputs[start_index2 + y];
          thread_node.input = m.inputs[start_index1 + x];

          thread_node.prob = m.probs[start_index1 + x] * m2.probs[start_index2 + y];
          write_index = x * num_elements2 + y;
          states[write_start_index +write_index] = thread_node;

          setOfStates.insert(std::make_tuple(m.target_states[start_index1 + x],m2.target_states[start_index2 + y]));
        }
      }
      write_start_index+= num_elements1 * num_elements2;
    }
  }


  std::set<std::tuple<int,int>>::iterator state_iterator;
/*
  for(state_iterator = setOfStates.begin(); state_iterator != setOfStates.end(); ++state_iterator){
    std::tuple<int,int> values = *state_iterator;
    int tmp_add_source = std::get<0>(values);
    int tmp_add_target = std::get<1>(values);

    if(map_data[tmp_add_source][tmp_add_target] > 0){
      stack_sources.push(values);
    }
  }
*/
  #if DEBUG
    printf("Going to for \n");
    printf("The size of the stack is %d \n",stack_sources.size());
  #endif

/*
  for(int check_target = 0;check_target < write_start_index; check_target++){
    int to_state_add =  states[check_target].to_state1;
    int to_state2_add = states[check_target].to_state2;
    if(map_data[to_state_add][to_state2_add] == 0){
      stack_sources.push(std::tie(to_state_add, to_state2_add));
      map_data[to_state_add][to_state2_add] = 1;
    }
  }
*/

  do{
    int start1 = std::get<0>(stack_sources.front());
    int start2 = std::get<1>(stack_sources.front());

    stack_sources.pop();


    write_start_index = total_elements;
    int index_start = total_elements;
    int offset_start = start1 * (m.num_outputs + 1);
    int offset_start2 = start2 * (m2.num_inputs + 1);


    int offset_vocab;
    int offset_vocab2;
    int add_term;
    for(int token = 0;token<m.num_outputs; token++){

      offset_vocab = offset_start + token;
      offset_vocab2 = offset_start2 + token;

      //Compute the offsets for the different states
      start_index1 = m.output_symbol_offsets[offset_vocab];
      start_index2 = m2.input_symbol_offsets[offset_vocab2];


      num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
      num_elements2 = m2.input_symbol_offsets[offset_vocab2+1] - start_index2;


      if(num_elements1 > 0 && num_elements2 > 0){
        //target, input, output, key
        std::map<int,std::map<int,std::map<int,float>>> edge_map;
        

        for(int x = 0; x < num_elements1; x++){
          for(int y=0; y< num_elements2; y++){
            thread_node.from_state = map_data[start1][start2];
            //thread_node.from_state1 = start1; //* m2.num_states + start2;
            //thread_node.from_state2 = start2;

            if(map_data[m.target_states[start_index1 + x]][m2.target_states[start_index2 + y]] == 0){
              stack_sources.push(std::tie(m.target_states[start_index1 + x], m2.target_states[start_index2 + y]));
              map_data[m.target_states[start_index1 + x]][m2.target_states[start_index2 + y]] = total_states;
              total_states++;
            }
            thread_node.to_state = map_data[m.target_states[start_index1 + x]][m2.target_states[start_index2 + y]];

            //thread_node.to_state1 = m.target_states[start_index1 + x]; //* m2.num_states + m2.target_states[start_index2 + y];
            //thread_node.to_state2 = m2.target_states[start_index2 + y];

            thread_node.output = m2.inputs[start_index2 + y];
            thread_node.input = m.inputs[start_index1 + x];

            edge_map[thread_node.to_state][thread_node.input][thread_node.output] = edge_map[thread_node.to_state][thread_node.input][thread_node.output] + (m.probs[start_index1 + x] * m2.probs[start_index2 + y]);


/*
            thread_node.prob = m.probs[start_index1 + x] + m2.probs[start_index2 + y];
            write_index = x * num_elements2 + y;
	    states[write_start_index +write_index] = thread_node;
*/
            //Debug statement
            if((write_start_index +write_index) < 0 || (write_start_index +write_index) > 1900000000){
              printf("THE VALUE IS %llu \n",(write_start_index +write_index));
              exit(0);
            }

          }
        }


        //Assign values of the final result and update total number of elements
        for(const auto& target : edge_map){
          for(const auto& input : target.second){
            for(const auto& output: input.second){
              thread_node.from_state = map_data[start1][start2];
              thread_node.to_state = target.first;
              thread_node.input = input.first;
              thread_node.output = output.first;
              thread_node.prob = output.second;
              states[write_start_index] = thread_node;
              write_start_index++;
              total_elements++;
            }
          }
        } 
        edge_map.clear();
        //write_start_index+= num_elements1 * num_elements2;
        //total_elements += num_elements1*num_elements2;
      }
    }


/*
  // THIS SECONT ATTEMPT GAVE A SLIGHTLY FASTER TUNNING TIME
  for(state_iterator = setOfStates.begin(); state_iterator != setOfStates.end(); ++state_iterator){
    std::tuple<int,int> values = *state_iterator;
    int tmp_add_source = std::get<0>(values);
    int tmp_add_target = std::get<1>(values);
    if(map_data[tmp_add_source][tmp_add_target] == 0){
      stack_sources.push(values);
      map_data[tmp_add_source][tmp_add_target] = 1;
    }
  }
*/

/*
 *  //THIS FIRST ATTEMPT GAVE THE LOWEST PERFORMANCE 
    for(int check_target = 0;check_target < write_start_index; check_target++){
      int to_state_add =  states[check_target].to_state1;
      int to_state2_add = states[check_target].to_state2;
      if(map_data[to_state_add][to_state2_add] == 0){
        stack_sources.push(std::tie(to_state_add, to_state2_add));
        map_data[to_state_add][to_state2_add] = 1;
      }
    }
*/

  }
  while(!stack_sources.empty());
  //Done adding states to the structure storing the composed transducer

  return total_elements;
}
