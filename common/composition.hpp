/**
  Author: Arturo Argueta
  This file contains the main methods to do composition on a GPU
**/
#include <tuple>
#include <stack>
#include <iostream>
#include <list>
#include <vector>
#include <omp.h>

int ceildiv(int x, int y) { return (x-1)/y+1; }

int composed_index(int index_1,int index_2,int num_states){return index_1*num_states+index_2;}

template <typename K, typename V>
V GetWithDef(const  std::map <K,V> & m, const K & key) {
   typename std::map<K,V>::const_iterator it = m.find( key );
   if ( it == m.end() ) {
      return -1;
   }
   else {
      return it->second;
   }
}

__device__ int gpu_ceildiv(int x, int y) { return (x-1)/y+1; }



__global__ void compute_symbol_composition(
  short* m_to_states,
  short* m_input,
  float* m_prob,
  short* m2_to_states,
  short* m2_output,
  float* m2_prob,
  int offset_start,
  int offset_size,
  int offset_start2,
  int offset_size2,
  composed_fst_node *gpu_states,
  int write_width,
  int write_start)
{
  int idx = blockIdx.x*blockDim.x+threadIdx.x;
  int idy = blockIdx.y*blockDim.y+threadIdx.y;

  int index_x;
  int index_y;

  //Placeholder node
  composed_fst_node thread_node = composed_fst_node(0);

  //Placeholder variables
  int to_state_x;
  int to_state_y;

  int write_index;
  //Make sure the threads only access edges that can be composed
  if(idx < offset_size && idy < offset_size2){
    index_x = offset_start + idx;
    index_y = offset_start2 + idy;

    to_state_x = m_to_states[index_x];
    to_state_y =  m2_to_states[index_y];
    thread_node.to_state1 = to_state_x;
    thread_node.to_state2 = to_state_y;
    thread_node.output = m2_output[index_y];
    thread_node.input = m_input[index_x];

    thread_node.prob = m_prob[index_x] + m2_prob[index_y];

    write_index = write_start + (idx * write_width + idy);
    gpu_states[write_index] = thread_node;
  }
}



__global__ void compute_symbol_composition_linear(
  short* m_to_states,
  short* m_output,
  float* m_prob,
  short input_to_state,
  int inputs_token,
  float input_probability,
  int offset_start,
  int offset_size,
  composed_fst_node *gpu_states)
{
  int idx = blockIdx.x*blockDim.x+threadIdx.x;
  int index_x;
  int to_state_x;

  //Placeholder node
  composed_fst_node thread_node = composed_fst_node(0);

  //Make sure the threads only access edges that can be composed
  if(idx < offset_size){
    index_x = offset_start + idx;

    to_state_x = m_to_states[index_x];
    thread_node.to_state1 = to_state_x;
    thread_node.to_state2 = input_to_state;
    thread_node.output = m_output[index_x];
    thread_node.input = inputs_token;
    thread_node.prob = m_prob[index_x] + input_probability;

    gpu_states[idx] = thread_node;
  }
}


/*
For this method, one kernel will be launched per expanded token given a source state pairs. The next method will launch 
one kernel per source state that wnts to be expanded
*/
unsigned long long int compute_efficient_composition(input_fst &m, output_fst &m2, composed_fst &test, composed_fst_node* states_host,cudaStream_t* streams){ //std::list<composed_fst_node> &statelist){

/*
  cudaStream_t streams[m.num_outputs];

  for(int i = 0; i< m.num_outputs; i++){
    cudaStreamCreate(&streams[i]);
  }
*/
  unsigned long long int total_elements = 0;
  int block_size = 32;
  dim3 threadsPerBlock(block_size,block_size);

  std::map< int,std::map<int,int> > map_data;
  map_data[0][0] = 1;

  //This structure will be used to make sure the correct source states get visited 
  std::stack< std::tuple<int,int> > stack_sources;

  //composed_fst_node* states;
  //cudaMallocHost((void**)&states, m2.num_states * m.num_states *sizeof(composed_fst_node));
  //states = (composed_fst_node*)malloc( m2.num_states * m.num_states *sizeof(composed_fst_node));

  //The number of outputs for the first transducer and inputs for the second one must match
  int states_width = max(m.num_outputs, m2.num_inputs);

  int num_elements1;
  int num_elements2;
  int start_index1;
  int start_index2;


  unsigned long long int write_start_index = 0;

  //compute states leaving state 0 from both transducers
  for(int offset_vocab = 0; offset_vocab < m.num_outputs; offset_vocab++){

    start_index1 = m.output_symbol_offsets[offset_vocab];
    start_index2 = m2.input_symbol_offsets[offset_vocab];

    num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
    num_elements2 = m2.input_symbol_offsets[offset_vocab+1] - start_index2;

    total_elements+=num_elements1*num_elements2;

    if(num_elements1 > 0 && num_elements2 > 0){
      dim3 numBlocks(ceildiv(num_elements1, block_size), ceildiv(num_elements2, block_size));

      compute_symbol_composition<<<numBlocks,threadsPerBlock,0,streams[offset_vocab]>>>(
        m.target_states.data().get(),
        m.inputs.data().get(),
        m.probs.data().get(),
        m2.target_states.data().get(),
        m2.inputs.data().get(),
        m2.probs.data().get(),
        start_index1,
        num_elements1,
        start_index2,
        num_elements2,
        test.gpu_states.data().get(),
        num_elements2,
        write_start_index
      );

/*
      cudaError err = cudaGetLastError();
      if ( cudaSuccess != err )
      {
        fprintf( stderr, "cudaCheckError kernel call () failed %s\n",cudaGetErrorString( err ) );
        exit( -1 );
      }
*/

      //Try stream copy here
      cudaMemcpyAsync(states_host+write_start_index, test.gpu_states.data().get()+write_start_index, (num_elements1*num_elements2)*sizeof(composed_fst_node), cudaMemcpyDeviceToHost,streams[offset_vocab]);     

      write_start_index+= num_elements1 * num_elements2;


      cudaError err = cudaGetLastError();
      if ( cudaSuccess != err )
      {
        printf("Copying %d elements and start %d \n",(num_elements1*num_elements2),write_start_index);
        fprintf( stderr, "cudaCheckError kernel () failed %s\n",cudaGetErrorString( err ) );
        exit( -1 );
      }

    } 
  }

  //Do a memcpy with all the composed elements 
  cudaDeviceSynchronize(); 

  cudaError copy_err = cudaGetLastError();
  if ( cudaSuccess != copy_err )
  {
    fprintf( stderr, "cudaCheckError() copy failed %s\n",cudaGetErrorString( copy_err ) );
    exit( -1 );
  }

  // Check the target states
  int target1,target2;
  int counter = 0;
  printf("Expanding %llu \n",write_start_index);
  for(int i=0;i< write_start_index;i++){
    target1 = states_host[i].to_state1;
    target2 = states_host[i].to_state2;
    //printf("-Pushing %d,%d \n",target1, target2);
    if(map_data[target1][target2] == 0){
      stack_sources.push(std::tie(target1, target2));


      //Push the state into the list
      //statelist.push_back(states[i]);
      map_data[target1][target2] = 1;
      counter++;
    }
  }

  printf("The size of the stack is %d \n",stack_sources.size());
  printf("The counter is %d and %llu \n",counter,total_elements); 
  printf("*The map size is %d \n",int(m.output_symbol_offset_map.size()));
  printf("*The second map size is %d \n",int(m2.input_symbol_offset_map.size()));


  // compute the transitions for the other states (in this case we will try to compose all states we can)
  do{
    //printf("Stack size %d \n",stack_sources.size());
    int start1 = std::get<0>(stack_sources.top());
    int start2 = std::get<1>(stack_sources.top());
    stack_sources.pop();

    write_start_index = 0;

    int offset_start = start1 * (m.num_outputs + 1);
    int offset_start2 = start2 * (m2.num_inputs + 1);


    int offset_vocab;
    int offset_vocab2;
    int add_term;    
    for(int token = 0;token<m.num_outputs; token++){

      offset_vocab = offset_start + token;
      offset_vocab2 = offset_start2 + token;

      //Compute the offsets for the different states
      start_index1 = m.output_symbol_offsets[offset_vocab];
      start_index2 = m2.input_symbol_offsets[offset_vocab2];


      num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
      num_elements2 = m2.input_symbol_offsets[offset_vocab2+1] - start_index2;


      //total_elements+=num_elements1*num_elements2;
      if(num_elements1 > 0 && num_elements2 > 0){
        //printf("number of expanded elements %llu \n",total_elements);
        add_term = num_elements1*num_elements2;

        int the_dim1 = ceildiv(num_elements1, block_size);
        int the_dim2 = ceildiv(num_elements2, block_size);
        dim3 numBlocks(the_dim1, the_dim2);

        compute_symbol_composition<<<numBlocks,threadsPerBlock,0,streams[token]>>>(
          m.target_states.data().get(),
          m.inputs.data().get(),
          m.probs.data().get(),
          m2.target_states.data().get(),
          m2.inputs.data().get(),
          m2.probs.data().get(),
          start_index1,
          num_elements1,
          start_index2,
          num_elements2,
          test.gpu_states.data().get(),
          num_elements2,
          write_start_index);


        //cudaDeviceSynchronize();
        cudaError err = cudaGetLastError();
        if ( cudaSuccess != err )
        {
          printf("THe dims: %d %d \n",the_dim1,the_dim2);
          printf("Copying %d elements and start %d \n",(num_elements1*num_elements2),write_start_index);
          fprintf( stderr, "cudaCheckError() kernel call failed %s\n",cudaGetErrorString( err ) );
          exit( -1 );
        }

        //Try async copy here
        cudaMemcpyAsync(states_host+write_start_index, test.gpu_states.data().get()+write_start_index, (num_elements1*num_elements2)*sizeof(composed_fst_node), cudaMemcpyDeviceToHost,streams[token]);

        write_start_index+= add_term;
        //printf("Adding %llu \n",add_term);

        err = cudaGetLastError();
        if ( cudaSuccess != err )
        {
          printf("Copying %d elements and start %d \n",(total_elements+write_start_index),(num_elements1*num_elements2));
          fprintf( stderr, "cudaCheckError() copy call failed %s\n",cudaGetErrorString( err ) );
          exit( -1 );
        }
      }
    }


    cudaDeviceSynchronize();

    #pragma omp parallel for shared(stack_sources,map_data) private(target1,target2)
    // Check the target states
    for(int i= 0/*total_elements*/; i< /*total_elements +*/  write_start_index;i++){
      //int id = omp_get_thread_num();
      //printf("Hello %d and size %d and i=%d\n",id,write_start_index,i);
      target1 = states_host[i].to_state1;
      target2 = states_host[i].to_state2;

      if(map_data[target1][target2] == 0){
        stack_sources.push(std::tie(target1, target2));
        map_data[target1][target2] = 1;
      }
    }
    //printf("Done pushing %d \n",stack_sources.size());

    //Update the start index where we can keep writing composed nodes/states
    total_elements += write_start_index;
  }
  while(!stack_sources.empty());

  return total_elements;
/*
  for(int i = 0; i< m.num_outputs; i++){
    cudaStreamDestroy(streams[i]);
  }
*/
}


/*
For this method, one kernel will be launched per expanded token given a source state pairs. The next method will launch 
one kernel per source state that wnts to be expanded
*/
int compute_linear_composition(input_fst_linear &m, composable_gpu_output &m2, composed_fst &test, composed_fst_node* states_host, const numberizer &inr, const numberizer &onr2){

  cudaStream_t streams[m.transitions];
  //printf("Create streams \n");
  for(int i = 0; i< m.transitions; i++){
    cudaStreamCreate(&streams[i]);
  }

  unsigned long long int total_elements = 0;
  int block_size = 32;
  dim3 threadsPerBlock(block_size,block_size);

  int num_elements;
  int start_index;

 
  for(int iteration =0 ; iteration < m.transitions; iteration++){
    //printf("During for \n");
    int symbol_index = m.output_symbols[iteration];

    //printf("Obtain Index %d - %d\n",symbol_index,int(m2.input_symbol_offsets.size()));
    start_index = m2.input_offsets[symbol_index];
    num_elements = m2.input_offsets[symbol_index+1] - start_index;
    //printf("After for \n");

    if(num_elements > 0){

      compute_symbol_composition_linear<<<ceildiv(num_elements, block_size), 32,0,streams[iteration]>>>(
        m2.to_states.data().get(),
        m2.outputs.data().get(),
        m2.probs.data().get(),
        m.target_states[iteration],
        m.input_symbols[iteration],
        m.probs[iteration],
        start_index,
        num_elements,
        test.gpu_states.data().get()
      );



      cudaError err = cudaGetLastError();
      if ( cudaSuccess != err )
      {
        fprintf( stderr, "cudaCheckError() failed %s\n",cudaGetErrorString( err ) );
        exit( -1 );
      }

    }


    cudaMemcpyAsync(states_host+total_elements, test.gpu_states.data().get() , num_elements*sizeof(composed_fst_node), cudaMemcpyDeviceToHost,streams[iteration]);


/*
cudaDeviceSynchronize();
//TODO: Remove this debugging section
printf("ITERATION \n");
for(int debug = total_elements; debug < total_elements+num_elements; debug++){
    composed_fst_node iter_node = states_host[debug];
    int to_state = iter_node.to_state1;
    int to_state2 = iter_node.to_state2;
    int output = iter_node.output;
    int input = iter_node.input;
    float prob = iter_node.prob;

    std::cout << "The node goes to " << to_state << " and " << to_state2 << " with input " << inr.num_to_word(input) << " and output " << onr2.num_to_word(output) << " and prob " << prob << std::endl;
}
*/

    total_elements+=num_elements;
    cudaError copy_err = cudaGetLastError();
    if ( cudaSuccess != copy_err )
    {
      fprintf( stderr, "cudaCheckError() copy failed %s\n",cudaGetErrorString( copy_err ) );
      exit( -1 );
    }

  }

  cudaDeviceSynchronize();
  return total_elements;
}

