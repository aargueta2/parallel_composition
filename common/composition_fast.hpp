/**
  Author: Arturo Argueta
  This file contains the main methods to do composition on a GPU
**/
#include <tuple>
#include <stack>
#include <queue>
#include <iostream>
#include <list>
#include <vector>
#include <stdint.h>
#include <thrust/sort.h>
#include <thrust/unique.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/execution_policy.h>
#include <thrust/reduce.h>
#include <thrust/functional.h>
#include <omp.h>

#include "scan.h"
#define DEBUG 1
#define MAX_BLOCK_SZ 1024
#define NUM_BANKS 32
#define LOG_NUM_BANKS 5

#ifdef ZERO_BANK_CONFLICTS
#define CONFLICT_FREE_OFFSET(n) \
    ((n) >> NUM_BANKS + (n) >> (2 * LOG_NUM_BANKS))
#else
#define CONFLICT_FREE_OFFSET(n) ((n) >> LOG_NUM_BANKS)
#endif


__device__ uint64_t combine(int32_t a, int16_t b, int16_t c)
{
    uint64_t a_64 = a;
    uint64_t b_64 = b;
    uint64_t c_64 = c;

    return (a_64 << 32) | (b_64 << 16) | (c_64);
}

inline
cudaError_t checkCuda(cudaError_t result)
{
#if defined(DEBUG) || defined(_DEBUG)
  if (result != cudaSuccess) {
    fprintf(stderr, "CUDA Runtime Error: %s\n",
            cudaGetErrorString(result));
    assert(result == cudaSuccess);
    cudaDeviceReset();
  }
#endif
  return result;
}

int ceildiv(int x, int y) { return (x-1)/y+1; }

int composed_index(int index_1,int index_2,int num_states){return index_1*num_states+index_2;}

struct state_tup{
  int first_state;
  int second_state;
};

struct hash_element{
  int composed_state;
  int assigned_flag;
};


/*
//SORT FUNCTIONS
__global__ void gpu_radix_sort_local2(compact_fst_node* d_out_sorted,
    unsigned int* d_prefix_sums,
    unsigned int* d_block_sums,
    unsigned int input_shift_width,
    compact_fst_node* d_in,
    unsigned int d_in_len,
    unsigned int max_elems_per_block)
{
    extern __shared__ char shmem2[];
    compact_fst_node* s_data = (compact_fst_node*)shmem2;

    unsigned int s_mask_out_len = max_elems_per_block + (max_elems_per_block >> LOG_NUM_BANKS);
    unsigned int* s_mask_out = (unsigned int*)(shmem2 + (max_elems_per_block*sizeof(compact_fst_node)));
    unsigned int* s_merged_scan_mask_out = s_mask_out + (s_mask_out_len);
    unsigned int* s_mask_out_sums = s_merged_scan_mask_out + (max_elems_per_block);
    unsigned int* s_scan_mask_out_sums = s_mask_out_sums+ (4);

    unsigned int thid = threadIdx.x;
    unsigned int cpy_idx = max_elems_per_block * blockIdx.x + thid;
    if (cpy_idx < d_in_len)
        s_data[thid] = d_in[cpy_idx];
    else
        s_data[thid].id = 0;

    __syncthreads();

    compact_fst_node t_data = s_data[thid];
    unsigned int t_2bit_extract = (t_data.id >> input_shift_width) & 3;

    for (unsigned int i = 0; i < 4; ++i)
    {

        s_mask_out[thid] = 0;
        if (thid + max_elems_per_block < s_mask_out_len)
            s_mask_out[thid + max_elems_per_block] = 0;
        __syncthreads();

        bool val_equals_i = false;
        if (cpy_idx < d_in_len)
        {
            val_equals_i = t_2bit_extract == i;
            s_mask_out[thid + CONFLICT_FREE_OFFSET(thid)] = val_equals_i;
        }
        __syncthreads();

        bool t_active = thid < (blockDim.x / 2);
        int offset = 1;
        for (int d = max_elems_per_block >> 1; d > 0; d >>= 1)
        {
            __syncthreads();

            if (t_active && (thid < d))
            {
                int ai = offset * ((thid << 1) + 1) - 1;
                int bi = offset * ((thid << 1) + 2) - 1;
                ai += CONFLICT_FREE_OFFSET(ai);
                bi += CONFLICT_FREE_OFFSET(bi);

                s_mask_out[bi] += s_mask_out[ai];
            }
            offset <<= 1;
        }

        if (thid == 0)
        {
            unsigned int total_sum = s_mask_out[max_elems_per_block - 1
                + CONFLICT_FREE_OFFSET(max_elems_per_block - 1)];
            s_mask_out_sums[i] = total_sum;
            d_block_sums[i * gridDim.x + blockIdx.x] = total_sum;
            s_mask_out[max_elems_per_block - 1
                + CONFLICT_FREE_OFFSET(max_elems_per_block - 1)] = 0;
        }
        __syncthreads();
        for (int d = 1; d < max_elems_per_block; d <<= 1)
        {
            offset >>= 1;
            __syncthreads();

            if (t_active && (thid < d))
            {
                int ai = offset * ((thid << 1) + 1) - 1;
                int bi = offset * ((thid << 1) + 2) - 1;
                ai += CONFLICT_FREE_OFFSET(ai);
                bi += CONFLICT_FREE_OFFSET(bi);

                unsigned int temp = s_mask_out[ai];
                s_mask_out[ai] = s_mask_out[bi];
                s_mask_out[bi] += temp;
            }
        }
        __syncthreads();

        if (val_equals_i && (cpy_idx < d_in_len))
        {
            s_merged_scan_mask_out[thid] = s_mask_out[thid + CONFLICT_FREE_OFFSET(thid)];
        }
        __syncthreads();
    }

    __syncthreads();
    if (thid == 0)
    {
        unsigned int run_sum = 0;
        for (unsigned int i = 0; i < 4; ++i)
        {
            s_scan_mask_out_sums[i] = run_sum;
            run_sum += s_mask_out_sums[i];
        }
    }
    __syncthreads();

    if (cpy_idx < d_in_len)
    {
        unsigned int new_pos = s_merged_scan_mask_out[thid] + s_scan_mask_out_sums[t_2bit_extract];
        unsigned int t_prefix_sum = s_merged_scan_mask_out[thid];
        __syncthreads();
        s_data[new_pos] = t_data;
        s_merged_scan_mask_out[new_pos] = t_prefix_sum;

        __syncthreads();

        d_prefix_sums[cpy_idx] = s_merged_scan_mask_out[thid];
        d_out_sorted[cpy_idx] = s_data[thid];
    }
}

__global__ void gpu_glbl_shuffle(compact_fst_node* d_out,
    compact_fst_node* d_in,
    unsigned int* d_scan_block_sums,
    unsigned int* d_prefix_sums,
    unsigned int input_shift_width,
    unsigned int d_in_len,
    unsigned int max_elems_per_block)
{
    unsigned int thid = threadIdx.x;
    unsigned int cpy_idx = max_elems_per_block * blockIdx.x + thid;

    if (cpy_idx < d_in_len)
    {
        compact_fst_node t_data = d_in[cpy_idx];
        unsigned int t_2bit_extract = (t_data.id >> input_shift_width) & 3;
        unsigned int t_prefix_sum = d_prefix_sums[cpy_idx];
        unsigned int data_glbl_pos = d_scan_block_sums[t_2bit_extract * gridDim.x + blockIdx.x]
            + t_prefix_sum;
        __syncthreads();
        d_out[data_glbl_pos] = t_data;
    }
}

void radix_sort(compact_fst_node* const d_out,
    compact_fst_node* const d_in,
    unsigned int d_in_len,
    unsigned int block_sz,
    unsigned int max_elems_per_block,
    unsigned int grid_sz,
    unsigned int* d_prefix_sums,
    unsigned int d_prefix_sums_len,
    unsigned int* d_block_sums,
    unsigned int d_block_sums_len,
    unsigned int* d_scan_block_sums,
    unsigned int s_data_len,
    unsigned int s_mask_out_len,
    unsigned int s_merged_scan_mask_out_len,
    unsigned int s_mask_out_sums_len,
    unsigned int s_scan_mask_out_sums_len,
    unsigned int shmem_sz)
{

    for (unsigned int shift_width = 0; shift_width <= 30; shift_width += 2)
    {
        gpu_radix_sort_local2<<<grid_sz, block_sz, shmem_sz>>>(d_out,
                                                                d_prefix_sums,
                                                                d_block_sums,
                                                                shift_width,
                                                                d_in,
                                                                d_in_len,
                                                                max_elems_per_block);


        sum_scan_blelloch(d_scan_block_sums, d_block_sums, d_block_sums_len);

        gpu_glbl_shuffle<<<grid_sz, block_sz>>>(d_in,
                                                    d_out,
                                                    d_scan_block_sums,
                                                    d_prefix_sums,
                                                    shift_width,
                                                    d_in_len,
                                                    max_elems_per_block);
    }

}


//END SORT FUNCTIONS
*/









__global__ void compute_symbol_composition_initial(
  short* m_to_states,
  short* m_input,
  float* m_prob,
  short* m2_to_states,
  short* m2_output,
  float* m2_prob,
  int offset_start,
  int offset_size,
  int offset_start2,
  int offset_size2,
  compact_fst_node *gpu_states,
  int num_inputs,
  int write_start,
  int num_states_fst1, //This argument contains the number of states in the composed transducer
  int num_states_fst2,
  hash_element* dev_hash_table, 
  state_tup* vertex_frontier, 
  int* dev_num_states,
  int source_1,
  int source_2,
  int* prob_table,
  int* duplicate_detector_flag
  )
{
  int idx = blockIdx.x*blockDim.x+threadIdx.x;
  int idy = blockIdx.y*blockDim.y+threadIdx.y;

  int index_x;
  int index_y;
  int write_state;

  //Placeholder node
  //compact_fst_node thread_node = compact_fst_node(0);

  int temp_add;
  //Placeholder variables
  int to_state_x;
  int to_state_y;
  int word_table_index;
  //int write_index;

  dev_hash_table[0].assigned_flag = 1;
  dev_hash_table[0].composed_state = 0;

  //Make sure the threads only access edges that can be composed
  if(idx < offset_size && idy < offset_size2){
    index_x = offset_start + idx;
    index_y = offset_start2 + idy;

    to_state_x = m_to_states[index_x];
    to_state_y =  m2_to_states[index_y];
    word_table_index = m2_output[index_y] * num_inputs + m_input[index_x];
    temp_add=atomicAdd(&prob_table[word_table_index], 1);

    if((temp_add+1) > 1){
      duplicate_detector_flag[0] = 1;
    }

    if(atomicOr(&dev_hash_table[to_state_x*num_states_fst2+to_state_y].assigned_flag,1) == 0){
      write_state = atomicAdd(dev_num_states, 1);
      dev_hash_table[to_state_x*num_states_fst2+to_state_y].composed_state = (write_state+num_states_fst1);

      //thread_node.to_state = (write_state+num_states_fst1);
      vertex_frontier[write_state].first_state = to_state_x;
      vertex_frontier[write_state].second_state = to_state_y;
    }
  }
}


__global__ void compute_symbols_initial(
  short* m_to_states,
  short* m_input,
  float* m_prob,
  short* m2_to_states,
  short* m2_output,
  float* m2_prob,
  int offset_start,
  int offset_size,
  int offset_start2,
  int offset_size2,
  compact_fst_node *gpu_states,
  unsigned long long *gpu_states_ids,
  int write_width,
  int write_start,
  int num_states_fst1, //This argument contains the number of states in the composed transducer
  int num_states_fst2,
  hash_element* dev_hash_table, 
  state_tup* vertex_frontier, 
  int* dev_num_states,
  int source_1,
  int source_2,
  int num_inputs,
  int* prob_table
  )
{
  int idx = blockIdx.x*blockDim.x+threadIdx.x;
  int idy = blockIdx.y*blockDim.y+threadIdx.y;

  int index_x;
  int index_y;

  //Placeholder node
  compact_fst_node thread_node;// = compact_fst_node(0);

  //Placeholder variables
  int to_state_x;
  int to_state_y;
  int write_index;


  //Make sure the threads only access edges that can be composed
  if(idx < offset_size && idy < offset_size2){
    index_x = offset_start + idx;
    index_y = offset_start2 + idy;

    to_state_x = m_to_states[index_x];
    to_state_y =  m2_to_states[index_y];

    write_index = write_start + (idx * write_width + idy);
    thread_node.output = m2_output[index_y];
    thread_node.input = m_input[index_x];
    thread_node.prob = m_prob[index_x] * m2_prob[index_y];
    thread_node.from_state = 0;
    thread_node.to_state = dev_hash_table[to_state_x*num_states_fst2+to_state_y].composed_state;

    //printf("--The to state (%d,%d) is %d \n",to_state_x,to_state_y,thread_node.to_state);
    gpu_states[write_index] = thread_node;
    gpu_states_ids[write_index] = combine(thread_node.to_state, thread_node.input, thread_node.output);
    //prob_table[thread_node.output * num_inputs + thread_node.input] = 0;
  }
}
















__global__ void compute_symbol_table(
  short* m_to_states,
  short* m_input,
  //float* m_prob,
  short* m2_to_states,
  short* m2_output,
  //float* m2_prob,
  int offset_start,
  int offset_size,
  int offset_start2,
  int offset_size2,
  //compact_fst_node *gpu_states,
  //int write_width,
  int num_inputs,
  int num_states_fst1, //This argument contains the number of states in the composed transducer
  int num_states_fst2,
  hash_element* dev_hash_table, 
  state_tup* vertex_frontier, 
  int* dev_num_states,
  int* prob_table,
  int* duplicate_detector_flag,
  int iteration_number
  //int source_1,
  //int source_2
  )
{
  int idx = blockIdx.x*blockDim.x+threadIdx.x;
  int idy = blockIdx.y*blockDim.y+threadIdx.y;

  int index_x;
  int index_y;
  int write_state;

  int temp_add;
  //Placeholder variables
  int to_state_x;
  int to_state_y;
  int word_table_index;

  //Make sure the threads only access edges that can be composed
  if(idx < offset_size && idy < offset_size2){
    index_x = offset_start + idx;
    index_y = offset_start2 + idy;

    to_state_x = int(m_to_states[index_x]);
    to_state_y =  int(m2_to_states[index_y]);

    word_table_index = m2_output[index_y] * num_inputs + m_input[index_x];
    temp_add = atomicExch(&prob_table[word_table_index], iteration_number);

    if(temp_add == iteration_number){
      duplicate_detector_flag[0] = 1;
    }

    if(atomicOr(&dev_hash_table[to_state_x*num_states_fst2+to_state_y].assigned_flag,1) == 0){
      write_state = atomicAdd(dev_num_states, 1);
      dev_hash_table[to_state_x*num_states_fst2+to_state_y].composed_state = write_state+num_states_fst1;
      //printf("Mapping %d %d to %d \n",to_state_x,to_state_y,int(write_state+num_states_fst1));

      vertex_frontier[write_state].first_state = to_state_x;
      vertex_frontier[write_state].second_state = to_state_y;
    }
  }
}





__global__ void compute_symbol_composition(
  short* m_to_states,
  short* m_input,
  float* m_prob,
  short* m2_to_states,
  short* m2_output,
  float* m2_prob,
  int offset_start,
  int offset_size,
  int offset_start2,
  int offset_size2,
  compact_fst_node *gpu_states,
  unsigned long long *gpu_states_ids,
  int write_width,
  int write_start,
  //int num_states_fst1, //This argument contains the number of states in the composed transducer
  int num_states_fst2,
  hash_element* dev_hash_table, 
  //state_tup* vertex_frontier, 
  //int* dev_num_states,
  int source_1,
  int source_2,
  int* prob_table,
  int num_inputs,
  int num_outputs
  )
{
  int idx = blockIdx.x*blockDim.x+threadIdx.x;
  int idy = blockIdx.y*blockDim.y+threadIdx.y;

  int index_x;
  int index_y;

  int source_state = dev_hash_table[source_1*num_states_fst2+source_2].composed_state;
  //Placeholder node
  compact_fst_node thread_node;// = compact_fst_node(0);

  //Placeholder variables
  int to_state_x;
  int to_state_y;
  int composed_to_state;
  //float temp_prob;

  int write_index;
  //Make sure the threads only access edges that can be composed
  if(idx < offset_size && idy < offset_size2){
    index_x = offset_start + idx;
    index_y = offset_start2 + idy;

    to_state_x = int(m_to_states[index_x]);
    to_state_y =  int(m2_to_states[index_y]);

    thread_node.output = m2_output[index_y];
    thread_node.input = m_input[index_x];
    thread_node.prob = m_prob[index_x] * m2_prob[index_y];
    thread_node.from_state = source_state;
    composed_to_state = dev_hash_table[to_state_x*num_states_fst2+to_state_y].composed_state;
    thread_node.to_state = composed_to_state;


    write_index = write_start + (idx * write_width + idy);

    gpu_states_ids[write_index] = combine(composed_to_state, thread_node.input, thread_node.output);
    gpu_states[write_index] = thread_node;

    //prob_table[thread_node.output * num_inputs + thread_node.input] = 0;
  }
}





__global__ void compute_edge_composition(
  short* m_to_states,
  short* m_input,
  float* m_prob,
  short* m2_to_states,
  short* m2_output,
  float* m2_prob,
  int offset_start,
  int offset_size,
  int offset_start2,
  int offset_size2,
  compact_fst_node *gpu_states,
  unsigned long long *gpu_states_ids,
  int write_width,
  int write_start,
  int num_states_fst1,
  int num_states_fst2,
  hash_element* dev_hash_table,
  state_tup* vertex_frontier, 
  int* dev_num_states,
  int source_1,
  int source_2,
  int* prob_table,
  int num_inputs,
  int num_outputs,
  int* duplicate_detector_flag,
  int iteration_number
)
{
  int idx = blockIdx.x*blockDim.x+threadIdx.x;
  int idy = blockIdx.y*blockDim.y+threadIdx.y;

  int index_x;
  int index_y;

  int source_state = dev_hash_table[source_1*num_states_fst2+source_2].composed_state;
  compact_fst_node thread_node;

  int write_state;

  int temp_add;
  //Placeholder variables
  int to_state_x;
  int to_state_y;
  int write_index;

  int word_table_index;

  //Make sure the threads only access edges that can be composed
  if(idx < offset_size && idy < offset_size2){
    index_x = offset_start + idx;
    index_y = offset_start2 + idy;

    to_state_x = int(m_to_states[index_x]);
    to_state_y =  int(m2_to_states[index_y]);

    thread_node.output = m2_output[index_y];
    thread_node.input = m_input[index_x];
    thread_node.prob = m_prob[index_x] + m2_prob[index_y];
    thread_node.from_state = source_state;


    word_table_index = m2_output[index_y] * num_inputs + m_input[index_x];
    temp_add = atomicExch(&prob_table[word_table_index], iteration_number);

    if(temp_add == iteration_number){
      duplicate_detector_flag[0] = 1;
    }

    if(atomicOr(&dev_hash_table[to_state_x*num_states_fst2+to_state_y].assigned_flag,1) == 0){
      write_state = atomicAdd(dev_num_states, 1);
      dev_hash_table[to_state_x*num_states_fst2+to_state_y].composed_state = (write_state+num_states_fst1);

      thread_node.to_state = (write_state+num_states_fst1);
      vertex_frontier[write_state].first_state = to_state_x;
      vertex_frontier[write_state].second_state = to_state_y;
    }
    else{
      thread_node.to_state = dev_hash_table[to_state_x*num_states_fst2+to_state_y].composed_state;
    }

    write_index = write_start + (idx * write_width + idy);
    gpu_states_ids[write_index] = combine(thread_node.to_state, thread_node.input, thread_node.output);
    gpu_states[write_index] = thread_node;

  }

}




/*
For this method, one kernel will be launched per expanded token given a source state pairs. The next method will launch 
one kernel per source state that wnts to be expanded
*/
unsigned long long int compute_efficient_composition(input_fst &m, output_fst &m2, fst_and_keys &gpu_states_raw, compact_fst_node* states_host,cudaStream_t* streams,hash_element* dev_hash_table, state_tup* vertex_frontier, int* dev_num_states,state_tup* stack_list,sort_pointers& sort_struct,int* prob_table){

  int *duplicate_detector_flag;
  int duplicate_detector_flag_host;
  cudaMalloc( (void**)&duplicate_detector_flag, sizeof(int));
  cudaMemset(duplicate_detector_flag, 0, sizeof(int));


  int num_sorts = 0;
  int host_num_states;
  unsigned long long int total_elements = 0;
 
  //unsigned long long max_expand_elements = 0;
  int block_size = 32;
  dim3 threadsPerBlock(block_size,block_size);

  //This structure will be used to make sure the correct source states get visited 
  std::queue< std::tuple<int,int> > stack_sources;

  //The number of outputs for the first transducer and inputs for the second one must match
  int states_width = max(m.num_outputs, m2.num_inputs);

  int num_elements1;
  int num_elements2;
  int start_index1;
  int start_index2;


  unsigned long long int write_start_index = 0;
  cudaMemset(dev_num_states,0,sizeof(int));

  int total_states_composition = 1;

  //compute edges leaving state 0 from both transducers
  for(int offset_vocab = 0; offset_vocab < m.num_outputs; offset_vocab++){

    cudaMemset(duplicate_detector_flag, 0, sizeof(int));

    start_index1 = m.output_symbol_offsets[offset_vocab];
    start_index2 = m2.input_symbol_offsets[offset_vocab];

    num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
    num_elements2 = m2.input_symbol_offsets[offset_vocab+1] - start_index2;

    total_elements+=num_elements1*num_elements2;
    //max_expand_elements = std::max(max_expand_elements,(unsigned long long)(num_elements1*num_elements2));

    if(num_elements1 > 0 && num_elements2 > 0){
      dim3 numBlocks(ceildiv(num_elements1, block_size), ceildiv(num_elements2, block_size));

      compute_symbol_composition_initial<<<numBlocks,threadsPerBlock,0,streams[offset_vocab]>>>(
        m.target_states.data().get(),
        m.inputs.data().get(),
        m.probs.data().get(),
        m2.target_states.data().get(),
        m2.inputs.data().get(),
        m2.probs.data().get(),
        start_index1,
        num_elements1,
        start_index2,
        num_elements2,
        gpu_states_raw.fst_edges.data().get(),
        m.num_inputs,
        write_start_index,
        total_states_composition, //number of states in the composed transducer
        m2.num_states,
        dev_hash_table, 
        vertex_frontier, 
        dev_num_states,
        0,
        0,
        prob_table,
        duplicate_detector_flag
      );

      compute_symbols_initial<<<numBlocks,threadsPerBlock,0,streams[offset_vocab]>>>(
        m.target_states.data().get(),
        m.inputs.data().get(),
        m.probs.data().get(),
        m2.target_states.data().get(),
        m2.inputs.data().get(),
        m2.probs.data().get(),
        start_index1,
        num_elements1,
        start_index2,
        num_elements2,
        gpu_states_raw.fst_edges.data().get(),
        gpu_states_raw.fst_edge_id.data().get(),
        num_elements2,
        write_start_index,
        total_states_composition, //number of states in the composed transducer
        m2.num_states,
        dev_hash_table,
        vertex_frontier,
        dev_num_states,
        0,
        0,
        m.num_inputs,
        prob_table
      );

      cudaError err = cudaGetLastError();
      if ( cudaSuccess != err )
      {
        fprintf( stderr, "cudaCheckError kernel call () failed %s\n",cudaGetErrorString( err ) );
        exit( -1 );
      }

      //Try stream copy here
      //cudaMemcpyAsync(states_host+write_start_index, test.gpu_states.data().get()+write_start_index, (num_elements1*num_elements2)*sizeof(composed_fst_node), cudaMemcpyDeviceToHost,streams[offset_vocab]);     

      write_start_index+= num_elements1 * num_elements2;

      err = cudaGetLastError();
      if ( cudaSuccess != err )
      {
        printf("Copying %d elements and start %d \n",(num_elements1*num_elements2),write_start_index);
        fprintf( stderr, "cudaCheckError kernel () failed %s\n",cudaGetErrorString( err ) );
        exit( -1 );
      }
    }   
  }

  cudaMemcpy(&duplicate_detector_flag_host, duplicate_detector_flag, sizeof(int), cudaMemcpyDeviceToHost);

  if(duplicate_detector_flag_host > 0){
    //Do sort
    thrust::sort_by_key(gpu_states_raw.fst_edge_id.data(), gpu_states_raw.fst_edge_id.data() + write_start_index, gpu_states_raw.fst_edges.data());
      
    //Do the reduction
    int size_length = (thrust::reduce_by_key(gpu_states_raw.fst_edge_id.data(), gpu_states_raw.fst_edge_id.data() + write_start_index, gpu_states_raw.fst_edges.data(), gpu_states_raw.fst_edge_id_aux.data(), gpu_states_raw.fst_edges_aux.data(), compact_node_equality(), reduce_compact_fst_node())).first  - gpu_states_raw.fst_edge_id_aux.data();

    cudaMemcpy(states_host, gpu_states_raw.fst_edges_aux.data().get(), (size_length)*sizeof(compact_fst_node), cudaMemcpyDeviceToHost);
    write_start_index = size_length;
  }
  else{
    cudaMemcpy(states_host, gpu_states_raw.fst_edges.data().get(), (write_start_index)*sizeof(compact_fst_node), cudaMemcpyDeviceToHost);
    num_sorts++;
  }


  //Do a memcpy with all the composed elements 
  cudaMemcpy(&host_num_states,dev_num_states,sizeof(int),cudaMemcpyDeviceToHost);
  cudaMemcpy(stack_list,vertex_frontier, host_num_states*sizeof(state_tup),cudaMemcpyDeviceToHost);
  total_states_composition += host_num_states;
  cudaMemset(dev_num_states,0,sizeof(int));

  cudaError copy_err = cudaGetLastError();
  if ( cudaSuccess != copy_err )
  {
    fprintf( stderr, "cudaCheckError() copy failed %s\n",cudaGetErrorString( copy_err ) );
    exit( -1 );
  }


  // Check the target states
  int target1,target2;
  for(int i=0;i<host_num_states; i++){
    stack_sources.push(std::tie(stack_list[i].first_state, stack_list[i].second_state));
  }


  #if DEBUG
    printf("The size of the parallel queue is %d \n",host_num_states);
    printf("The size of the stack is %d \n",stack_sources.size());
    printf("*The map size is %d \n",int(m.output_symbol_offset_map.size()));
    printf("*The second map size is %d \n",int(m2.input_symbol_offset_map.size()));
  #endif

  int num_sort = 0;
  int iteration_number = 1;

  // compute the transitions for the other states (in this case we will try to compose all states we can)
  do{
    cudaMemset(duplicate_detector_flag, 0, sizeof(int));

    int start1 = std::get<0>(stack_sources.front());
    int start2 = std::get<1>(stack_sources.front());
    stack_sources.pop();

    write_start_index = 0;
    

    int offset_start = start1 * (m.num_outputs + 1);
    int offset_start2 = start2 * (m2.num_inputs + 1);

    int offset_vocab;
    int offset_vocab2;
    int add_term;    
    for(int token = 0;token<m.num_outputs; token++){
      offset_vocab = offset_start + token;
      offset_vocab2 = offset_start2 + token;

      //Compute the offsets for the different states
      start_index1 = m.output_symbol_offsets[offset_vocab];
      start_index2 = m2.input_symbol_offsets[offset_vocab2];

      num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
      num_elements2 = m2.input_symbol_offsets[offset_vocab2+1] - start_index2;


      if(num_elements1 > 0 && num_elements2 > 0){

        int the_dim1 = ceildiv(num_elements1, block_size);
        int the_dim2 = ceildiv(num_elements2, block_size);
        dim3 numBlocks(the_dim1, the_dim2);



        compute_symbol_table<<<numBlocks,threadsPerBlock,0,streams[token]>>>(
          m.target_states.data().get(),
          m.inputs.data().get(),
          //m.probs.data().get(),
          m2.target_states.data().get(),
          m2.inputs.data().get(),
          //m2.probs.data().get(),
          start_index1,
          num_elements1,
          start_index2,
          num_elements2,
          //test.gpu_states.data().get(),
          //num_elements2,
          m.num_inputs,
          total_states_composition, // Number of states in the composed transducer
          m2.num_states,
          dev_hash_table,
          vertex_frontier,
          dev_num_states,
          prob_table,
          duplicate_detector_flag,
          iteration_number
          //start1,
          //start2
          );


        compute_symbol_composition<<<numBlocks,threadsPerBlock,0,streams[token]>>>(
          m.target_states.data().get(),
          m.inputs.data().get(),
          m.probs.data().get(),
          m2.target_states.data().get(),
          m2.inputs.data().get(),
          m2.probs.data().get(),
          start_index1,
          num_elements1,
          start_index2,
          num_elements2,
          gpu_states_raw.fst_edges.data().get(),
          gpu_states_raw.fst_edge_id.data().get(),
          num_elements2,
          write_start_index,
          //total_states_composition, // Number of states in the composed transducer
          m2.num_states,
          dev_hash_table,
          //vertex_frontier,     
          //dev_num_states,
          start1,
          start2,
          prob_table,
          m.num_inputs,
          m2.num_outputs
          );

        add_term = num_elements1*num_elements2;
        write_start_index+= add_term;

      }
    }

   // cudaDeviceSynchronize();
    cudaMemcpy(&duplicate_detector_flag_host, duplicate_detector_flag, sizeof(int), cudaMemcpyDeviceToHost);
    iteration_number++;


    if(duplicate_detector_flag_host > 0){
      num_sort++;
      num_sorts++;

      //Do sort
      thrust::sort_by_key(gpu_states_raw.fst_edge_id.data(), gpu_states_raw.fst_edge_id.data() + write_start_index, gpu_states_raw.fst_edges.data());
      
      //Do the reduction
      int size_length = (thrust::reduce_by_key(gpu_states_raw.fst_edge_id.data(), gpu_states_raw.fst_edge_id.data() + write_start_index, gpu_states_raw.fst_edges.data(), gpu_states_raw.fst_edge_id_aux.data(), gpu_states_raw.fst_edges_aux.data(), compact_node_equality(), reduce_compact_fst_node())).first  - gpu_states_raw.fst_edge_id_aux.data();
  

       //This part will be used to check the input edges
      #if 0
      if(0){//size_length != write_start_index){
      printf("START 1 %d and START 2 %d \n",start1,start2); 
      compact_fst_node* temp_edge_check;
      temp_edge_check = (compact_fst_node*)malloc(write_start_index* sizeof(compact_fst_node));

      unsigned long long* temp_id_edge_check;
      temp_id_edge_check = (unsigned long long*)malloc(write_start_index* sizeof(unsigned long long));

      
      cudaMemcpy(temp_edge_check,gpu_states_raw.fst_edges.data().get(), write_start_index * sizeof(compact_fst_node), cudaMemcpyDeviceToHost);
      cudaMemcpy(temp_id_edge_check, gpu_states_raw.fst_edge_id.data().get(), write_start_index * sizeof(unsigned long long), cudaMemcpyDeviceToHost);

      std::map<short,std::map<short, std::map<short,float>>> copy_check;
      std::map<unsigned long long, int> copy_id_check;
      for(int test = 0; test < write_start_index; ++test){
        compact_fst_node temp_node = temp_edge_check[test];

        copy_id_check[temp_id_edge_check[test]] = 0;
        copy_check[temp_node.to_state][temp_node.output][temp_node.input] = 0;
      }

      for(int test = 0; test < write_start_index; ++test){
  
        compact_fst_node temp_node = temp_edge_check[test];
        if(copy_id_check[temp_id_edge_check[test]] == 0){
          printf("--From state %hu To state %hu output %hu input %hu probability %f and id %llu \n",temp_node.from_state,temp_node.to_state,temp_node.output,temp_node.input,temp_node.prob, temp_id_edge_check[test]);
          // temp_id_edge_check[test] = 1;
          copy_id_check[temp_id_edge_check[test]] = 1;
        }
        else{
          printf("**From state %hu To state %hu output %hu input %hu probability %f and id %llu \n",temp_node.from_state,temp_node.to_state,temp_node.output,temp_node.input,temp_node.prob,temp_id_edge_check[test]);
        }
      }


      printf("The regular size is %d and the reduced size %d \n",write_start_index,size_length);
printf("------------///////////------------ \n");
      }
    #endif

      cudaMemcpy(states_host+total_elements, gpu_states_raw.fst_edges_aux.data().get(), (size_length)*sizeof(compact_fst_node), cudaMemcpyDeviceToHost);
      write_start_index = size_length;

    }
    else{


        cudaMemcpy(states_host+total_elements, gpu_states_raw.fst_edges.data().get(), (write_start_index)*sizeof(compact_fst_node), cudaMemcpyDeviceToHost);
        num_sorts++;
   }

   
    //Error checkpoint 
    cudaError err = cudaGetLastError();
    if ( cudaSuccess != err )
    {
      printf("Copying %d elements and start %d \n",(total_elements+write_start_index),(num_elements1*num_elements2));
      fprintf( stderr, "cudaCheckError() copy call failed %s\n",cudaGetErrorString( err ) );
      exit( -1 );
    }

    //Do a memcpy with all the composed elements 
    cudaMemcpy(&host_num_states,dev_num_states,sizeof(int),cudaMemcpyDeviceToHost);
    cudaMemcpy(stack_list,vertex_frontier, host_num_states*sizeof(state_tup),cudaMemcpyDeviceToHost);
    cudaMemset(dev_num_states,0,sizeof(int));

    //Update the number of states on the composed transducer
    total_states_composition += host_num_states;

    #pragma omp parallel for shared(stack_sources) private(target1,target2)
    // Check the target states
    for(int i= 0; i< host_num_states;i++){
      target1 = stack_list[i].first_state;
      target2 = stack_list[i].second_state;
      stack_sources.push(std::tie(target1, target2));
    }

    //Update the start index where we can keep writing composed nodes/states
    total_elements += write_start_index;
  }
  while(!stack_sources.empty());

  #if DEBUG
    std::cout << "THE NUMBER OF SORTS IS  " << num_sort << " and the total is " << num_sorts << std::endl;
  #endif

  return total_elements;
}


//Delete this code
/*
          compute_edge_composition<<<numBlocks,threadsPerBlock,0,streams[token]>>>(
          m.target_states.data().get(),
          m.inputs.data().get(),
          m.probs.data().get(),
          m2.target_states.data().get(),
          m2.inputs.data().get(),
          m2.probs.data().get(),
          start_index1,
          num_elements1,
          start_index2,
          num_elements2,
          gpu_states_raw.fst_edges.data().get(),
          gpu_states_raw.fst_edge_id.data().get(),
          num_elements2,
          write_start_index,
          total_states_composition,
          m2.num_states,
          dev_hash_table,
          vertex_frontier,
          dev_num_states,
          start1,
          start2,
          prob_table,
          m.num_inputs,
          m2.num_outputs,
          duplicate_detector_flag,
          iteration_number
          );
*/



/*
//TEST SORT
    radix_sort(
      sort_struct.d_out,
      gpu_states_raw,
      (unsigned int)write_start_index,
      sort_struct.block_sz,
      sort_struct.max_elems_per_block,
      sort_struct.grid_sz,
      sort_struct.d_prefix_sums,
      sort_struct.d_prefix_sums_len,
      sort_struct.d_block_sums,
      sort_struct.d_block_sums_len,
      sort_struct.d_scan_block_sums,
      sort_struct.s_data_len,
      sort_struct.s_mask_out_len,
      sort_struct.s_merged_scan_mask_out_len,
      sort_struct.s_mask_out_sums_len,
      sort_struct.s_scan_mask_out_sums_len,
      sort_struct.shmem_sz);
*/
  //cudaMemcpy(states_host, gpu_states_raw.fst_edges.data().get(), (write_start_index)*sizeof(compact_fst_node), cudaMemcpyDeviceToHost);

/*
    for(int token = 0;token<m.num_outputs; token++){
      offset_vocab = offset_start + token;
      offset_vocab2 = offset_start2 + token;

      //Compute the offsets for the different states
      start_index1 = m.output_symbol_offsets[offset_vocab];
      start_index2 = m2.input_symbol_offsets[offset_vocab2];


      num_elements1 = m.output_symbol_offsets[offset_vocab+1] - start_index1;
      num_elements2 = m2.input_symbol_offsets[offset_vocab2+1] - start_index2;


      if(num_elements1 > 0 && num_elements2 > 0){

        add_term = num_elements1*num_elements2;

        int the_dim1 = ceildiv(num_elements1, block_size);
        int the_dim2 = ceildiv(num_elements2, block_size);
        dim3 numBlocks(the_dim1, the_dim2);
 
        compute_symbol_composition<<<numBlocks,threadsPerBlock,0,streams[token]>>>(
          m.target_states.data().get(),
          m.inputs.data().get(),
          m.probs.data().get(),
          m2.target_states.data().get(),
          m2.inputs.data().get(),
          m2.probs.data().get(),
          start_index1,
          num_elements1,
          start_index2,
          num_elements2,
          gpu_states_raw.fst_edges.data().get(),
          gpu_states_raw.fst_edge_id.data().get(),
          num_elements2,
          write_start_index,
          //total_states_composition, // Number of states in the composed transducer
          m2.num_states,
          dev_hash_table,
          //vertex_frontier,     
          //dev_num_states,
          start1,
          start2,
          prob_table,
          m.num_inputs,
          m2.num_outputs
          );

        write_start_index+= add_term;

        cudaError err = cudaGetLastError();
        if ( cudaSuccess != err )
        {
          printf("Copying %d elements and start %d \n",(total_elements+write_start_index),(num_elements1*num_elements2));
          fprintf( stderr, "cudaCheckError() copy call failed %s\n",cudaGetErrorString( err ) );
          exit( -1 );
        }
      }
    }

    cudaDeviceSynchronize();
*/


