#include <fstream>
#include <sstream>

#include "fst.hpp"

#define LOG 0

fst read_fst(const std::string &filename, const numberizer &inr, const numberizer &onr) {
  using namespace std;

  fst m;
  ifstream fst_file(filename);
  string line;
  bool first = true;

  m.num_inputs = inr.size();
  m.num_outputs = onr.size();

  while (getline(fst_file, line)) {
    // Count number of fields
    istringstream iss(line);
    string token;
    vector<string> tokens;
    while (iss >> token)
      tokens.push_back(token);
 

    // Transition
    iss.str(line); iss.clear();
    state_t q, r;
    string fstr, estr;
    sym_t f, e;
    prob_t p;

    if (tokens.size() == 5) {
      iss >> q >> r >> fstr >> estr >> p;
      f = inr.word_to_num(fstr);
      e = onr.word_to_num(estr);
      if (first) {
	m.initial = q;
	first = false;
      }

      #if LOG
        m.add_transition(q, r, f, e, log(p));
      #else
        m.add_transition(q, r, f, e, p);
      #endif

    // Final state
    } else if (tokens.size() == 2) {
      iss >> q >> p;
      if (first) {
	m.initial = q;
	first = false;
      }
      #if LOG
        m.add_final(q, log(p));
      #else
        m.add_final(q,p);
      #endif

    } else {
      throw runtime_error("wrong number of fields in line");
    }
  }
  return m;
}

fst read_fst_csc(const std::string &filename, const numberizer &inr, const numberizer &onr,float neg) {
  using namespace std;

  fst m;
  ifstream fst_file(filename);
  string line;
  bool first = true;

  m.num_inputs = inr.size();
  m.num_outputs = onr.size();

  while (getline(fst_file, line)) {
    // Count number of fields
    istringstream iss(line);
    string token;
    vector<string> tokens;
    while (iss >> token)
      tokens.push_back(token);

    // Transition
    iss.str(line); iss.clear();
    state_t q, r;
    string fstr, estr;
    sym_t f, e;
    prob_t p;

    if (tokens.size() == 5) {
      // switch q and r to make it regular csr
      iss >> r >> q >> fstr >> estr >> p;
      f = inr.word_to_num(fstr);
      e = onr.word_to_num(estr);
      if (first) {
        m.initial = q;
        first = false;
      }
      m.add_transition(q, r, f, e, neg*log(p));

    // Final state
    } else if (tokens.size() == 2) {
      iss >> q >> p;
      if (first) {
        m.initial = q;
        first = false;
      }
      m.add_final(q, neg*log(p));

    } else {
      throw runtime_error("wrong number of fields in line");
    }
  }
  return m;
}


fst read_fst_noLog(const std::string &filename, const numberizer &inr, const numberizer &onr) {
  using namespace std;

  fst m;
  ifstream fst_file(filename);
  string line;
  bool first = true;

  m.num_inputs = inr.size();
  m.num_outputs = onr.size();

  while (getline(fst_file, line)) {
    // Count number of fields
    istringstream iss(line);
    string token;
    vector<string> tokens;
    while (iss >> token)
      tokens.push_back(token);

    // Transition
    iss.str(line); iss.clear();
    state_t q, r;
    string fstr, estr;
    sym_t f, e;
    prob_t p;

    if (tokens.size() == 5) {
      iss >> q >> r >> fstr >> estr >> p;
      f = inr.word_to_num(fstr);
      e = onr.word_to_num(estr);
      if (first) {
	m.initial = q;
	first = false;
      }
      m.add_transition(q, r, f, e, p);

    // Final state
    } else if (tokens.size() == 2) {
      iss >> q >> p;
      if (first) {
	m.initial = q;
	first = false;
      }
      m.add_final(q, p);

    } else {
      throw runtime_error("wrong number of fields in line");
    }
  }
  return m;
}


fst_composed_probs read_fst_exp_mantissa(const std::string &filename, const numberizer &inr, const numberizer &onr) {
  using namespace std;

  fst_composed_probs m;
  ifstream fst_file(filename);
  string line;
  bool first = true;

  m.num_inputs = inr.size();
  m.num_outputs = onr.size();

  while (getline(fst_file, line)) {
    // Count number of fields
    istringstream iss(line);
    string token;
    vector<string> tokens;
    while (iss >> token)
      tokens.push_back(token);

    // Transition
    iss.str(line); iss.clear();
    state_t q, r;
    string fstr, estr;
    sym_t f, e;
    prob_t p;
    exponent ee;
    mantissa man;

    if (tokens.size() == 5) {
      iss >> q >> r >> fstr >> estr >>  p;
      man = frexp(p,&ee);
      f = inr.word_to_num(fstr);
      e = onr.word_to_num(estr);
      if (first) {
	m.initial = q;
	first = false;
      }
      m.add_transition(q, r, f, e, p, ee, man);

    // Final state
    } else if (tokens.size() == 2) {
      iss >> q >> p;
      man = frexp(p,&ee);
      if (first) {
	m.initial = q;
	first = false;
      }
      m.add_final(q, p, ee, man);

    } else {
      throw runtime_error("wrong number of fields in line");
    }
  }
  return m;
}

std::vector<int> read_input_tokens(const std::string &filename, const numberizer &in_numberizer){
  using namespace std;
  vector<int> tokens;
  ifstream fst_file(filename);
  string line;

  while (getline(fst_file, line)) {
    istringstream iss(line);
    string token;
    while (iss >> token){
      tokens.push_back(in_numberizer.word_to_num(token));
    }
  }

  return tokens;
}


bool compare_input (const transition_t &x, const transition_t &y) {
  return std::get<2>(x) < std::get<2>(y);
}

bool compare_fromstate_output(const transition_t &t1, const transition_t &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  prob_t p1, p2;
  std::tie(q1, r1, f1, e1, p1) = t1;
  std::tie(q2, r2, f2, e2, p2) = t2;
  return q1 < q2 || q1 == q2 && e1 < e2;
}

bool compare_fromstate_input(const transition_t &t1, const transition_t &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  prob_t p1, p2;
  std::tie(q1, r1, f1, e1, p1) = t1;
  std::tie(q2, r2, f2, e2, p2) = t2;
  return q1 < q2 || q1 == q2 && f1 < f2;
}



bool compare_fromstate_tostate(const transition_t &t1, const transition_t &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  prob_t p1, p2;
  std::tie(q1, r1, f1, e1, p1) = t1;
  std::tie(q2, r2, f2, e2, p2) = t2;
  return q1 < q2;
}

bool compare_input_fromstate_tostate(const transition_t &t1, const transition_t &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  prob_t p1, p2;
  std::tie(q1, r1, f1, e1, p1) = t1;
  std::tie(q2, r2, f2, e2, p2) = t2;
  return f1 < f2 || (f1 == f2 && q1 < q2) || (f1 == f2 && q1 == q2 && r1 < r2);
}

bool compare_output_fromstate_tostate(const transition_t &t1, const transition_t &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  prob_t p1, p2;
  std::tie(q1, r1, f1, e1, p1) = t1;
  std::tie(q2, r2, f2, e2, p2) = t2;
  return e1 < e2 || (e1 == e2 && q1 < q2) || (e1 == e2 && q1 == q2 && r1 < r2);
}


bool compare_input_tostate_fromstate(const transition_t &t1, const transition_t &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  prob_t p1, p2;
  std::tie(q1, r1, f1, e1, p1) = t1;
  std::tie(q2, r2, f2, e2, p2) = t2;
  return f1 < f2 || (f1 == f2 && r1 < r2) || (f1 == f2 && r1 == r2 && q1 < q2);
}

bool compare_input_em(const transition_float &x, const transition_float &y) {
  return std::get<2>(x) < std::get<2>(y);
}

bool compare_input_fromstate_tostate_em(const transition_float &t1, const transition_float &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  exponent ee1, ee2;
  prob_t p1,p2;
  mantissa m1, m2;
  std::tie(q1, r1, f1, e1, p1, ee1, m1) = t1;
  std::tie(q2, r2, f2, e2, p2, ee2, m2) = t2;
  return f1 < f2 || (f1 == f2 && q1 < q2) || (f1 == f2 && q1 == q2 && r1 < r2);
}



bool compare_output_fromstate_tostate_em(const transition_float &t1, const transition_float &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  exponent ee1, ee2;
  prob_t p1,p2;
  mantissa m1, m2;
  std::tie(q1, r1, f1, e1, p1, ee1, m1) = t1;
  std::tie(q2, r2, f2, e2, p2, ee2, m2) = t2;
  return f1 < f2 || (f1 == f2 && q1 < q2) || (f1 == f2 && q1 == q2 && r1 < r2);
}


bool compare_input_tostate_fromstate_em(const transition_float &t1, const transition_float &t2) {
  state_t q1, r1, q2, r2;
  sym_t f1, e1, f2, e2;
  exponent ee1, ee2;
  prob_t p1,p2;
  mantissa m1, m2;
  std::tie(q1, r1, f1, e1, p1, ee1, m1) = t1;
  std::tie(q2, r2, f2, e2, p2, ee2, m2) = t2;
  return f1 < f2 || (f1 == f2 && r1 < r2) || (f1 == f2 && r1 == r2 && q1 < q2);
}

// Workaround for bug in nvcc

void sort_by_input(fst &m) {
  std::sort(m.transitions.begin(), m.transitions.end(), compare_input);
}

void sort_fromstate_tostate(fst &m) {
  std::sort(m.transitions.begin(), m.transitions.end(), compare_fromstate_tostate);
}

// Sorting used for composition
void sort_by_fromstate_input(fst &m) {
  std::sort(m.transitions.begin(), m.transitions.end(), compare_fromstate_input);
}
void sort_by_fromstate_output(fst &m) {
  std::sort(m.transitions.begin(), m.transitions.end(), compare_fromstate_output);
}

void sort_by_input_fromstate_tostate(fst &m) {
  std::sort(m.transitions.begin(), m.transitions.end(), compare_input_fromstate_tostate);
}

void sort_by_output_fromstate_tostate(fst &m) {
  std::sort(m.transitions.begin(), m.transitions.end(), compare_output_fromstate_tostate);
}

void sort_by_input_tostate_fromstate(fst &m) {
  std::sort(m.transitions.begin(), m.transitions.end(), compare_input_tostate_fromstate);
}
//em methods
void sort_by_input_em(fst_composed_probs &m) {
  std::sort(m.transition_f.begin(), m.transition_f.end(), compare_input_em);
}

void sort_by_input_fromstate_tostate_em(fst_composed_probs &m) {
  std::sort(m.transition_f.begin(), m.transition_f.end(), compare_input_fromstate_tostate_em);
}

void sort_by_input_tostate_fromstate_em(fst_composed_probs &m) {
  std::sort(m.transition_f.begin(), m.transition_f.end(), compare_input_tostate_fromstate_em);
}
