#ifndef GPU_FST_HPP
#define GPU_FST_HPP

#include <vector>
#include <iostream>
#include <cfloat>
#include "fst.hpp"
#include "numberizer.hpp"


struct input_fst{
  state_t initial;
  std::vector<int> source_states;
  std::vector<int> output_symbol_offsets;
  
  std::vector<int> target_states;
  std::vector<int> inputs;
  std::vector<float> probs;

  state_t num_states;
  sym_t num_inputs, num_outputs;

  //constructor for the fst representation
  input_fst(fst &&m)
    :
    initial(m.initial),
    num_states(m.num_states),
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    source_states(m.num_states+1),
    output_symbol_offsets((m.num_outputs+1) * m.num_states),
    target_states(m.transitions.size()),
    inputs(m.transitions.size()),
    probs(m.transitions.size())
  {
    //Sort edge transitions accordingly
    sort_by_fromstate_output(m);

    // Begin filling data structure
    sym_t e_last = -1;
    int q_last = 0;
    int compute_index = 0;
    for (int i=0; i<m.transitions.size(); i++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];
      target_states[i] = r;
      inputs[i] = f;
      probs[i] = p;

      //printf("Transition %d %d %d %d \n",q,r,f,e);
      //Keep track of the current state read
      if(q > q_last) {
        for(int x = e_last+1; x < m.num_outputs+1; x++){
          compute_index = q_last * (m.num_outputs+1) + x;
          output_symbol_offsets[compute_index] = i;
        }
        q_last = q;
        e_last = -1;
      }

      //Keep track of the output labels that are read
      while(e > e_last){
        e_last++;
        compute_index = q_last * (m.num_outputs+1) + e_last;
        //output_symbol_offset_map[q_last][e_last] = i;
        output_symbol_offsets[compute_index] = i;
      }
    }
    //printf("Begin unzip \n");
    for(int i= (q_last * (m.num_outputs+1) + e_last)+1; i< (m.num_outputs+1) * m.num_states; i++)
    {
      output_symbol_offsets[i] = m.transitions.size();
    }
  }
};




struct output_fst{
  state_t initial;
  std::vector<int> source_states;
  std::vector<int> input_symbol_offsets;
  //thrust::device_vector<int> input_symbol_offsets_device;
  //std::map< int,std::map<int,int> > input_symbol_offset_map;
 
  std::vector<int> target_states;
  std::vector<int> inputs;
  std::vector<float> probs;

  state_t num_states;
  sym_t num_inputs, num_outputs;

  //constructor for the fst representation
  output_fst(fst &&m)
    :
    initial(m.initial),
    num_states(m.num_states),
    num_inputs(m.num_inputs),
    num_outputs(m.num_outputs),
    source_states(m.num_states+1),
    input_symbol_offsets((m.num_inputs+1) * m.num_states),
    target_states(m.transitions.size()),
    inputs(m.transitions.size()),
    probs(m.transitions.size())
  {

    //Sort edge transitions accordingly
    sort_by_fromstate_input(m);

    // Begin filling data structure
    sym_t f_last = -1;
    int q_last = 0;
    int compute_index = 0;
    for (int i=0; i<m.transitions.size(); i++) {
      state_t q, r;
      sym_t f, e;
      prob_t p;
      std::tie(q, r, f, e, p) = m.transitions[i];
      target_states[i] = r;
      inputs[i] = e;
      probs[i] = p;
      //printf("Transition %d %d %d %d \n",q,r,f,e);

      //Keep track of the current state read
      if(q > q_last) {
        for(int x = f_last+1; x < m.num_inputs+1; x++){
          compute_index = q_last * (m.num_inputs+1) + x;
          input_symbol_offsets[compute_index] = i;
        }
        q_last = q;
        f_last = -1;
      }

      //Keep track of the output labels that are read
      while(f > f_last){
        f_last++;
        compute_index = q_last * (m.num_inputs+1) + f_last;
        //input_symbol_offset_map[q][f_last] = i;
        input_symbol_offsets[compute_index] = i;
      }
    }

    for(int i= (q_last * (m.num_inputs+1) + f_last)+1; i< (m.num_inputs+1) * m.num_states; i++)
    {
      input_symbol_offsets[i] = int(m.transitions.size());
    }
  }
};


struct composed_fst_node{
  int from_state1;
  int to_state1;
 
  int from_state2;
  int to_state2;
  int output;
  int input;

  float prob;

  composed_fst_node():
  //from_state1(),
  //from_state2(),
  to_state1(),
  to_state2(),
  output(),
  input(),
  prob(-FLT_MAX)
  {
  }
 
  composed_fst_node(int from,int to, int out, int in, float p):
  //from_state1(from),
  //from_state2(from),
  to_state1(to),
  to_state2(to),
  output(out),
  input(in),
  prob(p)
  {}
};





struct compact_fst_node{
  int from_state;
  int to_state;
  int output;
  int input;
  float prob;

  compact_fst_node():
  from_state(),
  to_state(),
  output(),
  input(),
  prob(-FLT_MAX)
  {
  }
 
  compact_fst_node(int from,int to, int out, int in, float p):
  from_state(from),
  to_state(to),
  output(out),
  input(in),
  prob(p)
  {}
};


struct composed_fst{
  int num_nodes;
  composed_fst_node reset_state;
  composed_fst_node* states;
  composed_fst_node* gpu_states;

  composed_fst(int in_nodes):
  num_nodes(in_nodes)
  {
    states = (composed_fst_node *) malloc( in_nodes * sizeof(composed_fst_node) );

    for(int foo=0;foo<in_nodes;foo++){
      //states[foo].from_state1 = 0;
      //states[foo].from_state2 = 0;
      states[foo].to_state1 = 0;
      states[foo].to_state2 = 0;
      states[foo].output = 0;
      states[foo].input = 0;

      states[foo].prob = -FLT_MAX;
    }

    // Reset state
    reset_state.to_state1 = 0;
    reset_state.to_state2 = 0;
    reset_state.output = 0;
    reset_state.input = 0;
    reset_state.prob = -FLT_MAX;
  }

};

bool operator< ( composed_fst_node a1, composed_fst_node b1 ) { return (a1.from_state1+a1.to_state2+a1.input+a1.output) < (b1.from_state1+b1.to_state2+b1.input+b1.output) ; }
#endif
