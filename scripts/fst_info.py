import sys
import numpy as np

#Read file containing fst in att format
fst_file = open(sys.argv[1],'rw')

max_state = 0
tup_list = set()
#read the file
for line in fst_file:
	tokens = line.split()
	source_state = int(tokens[0])
	target_state = int(tokens[1])
        tup_list.add((source_state,target_state))
        if source_state > max_state:
		max_state = source_state
	if target_state > max_state:
		max_state = target_state

max_state += 1
fst_matrix = np.zeros((max_state, max_state))

for elem in tup_list:
	src_state = elem[0]
	trg_state = elem[1]
	fst_matrix[src_state][trg_state] = 1

num_zero_elem = 0
num_non_zero_elem = 0
for x in range(0,max_state):
	for y in range(0,max_state):
		if fst_matrix[x][y] == 0:
			num_zero_elem+=1
		else:
			num_non_zero_elem+=1

print len(tup_list)
print num_zero_elem
print num_non_zero_elem
print max_state
print max_state*max_state
total_cells = max_state*max_state
print "The sparsity percent is "+str(float(num_zero_elem)/float(total_cells))
#TODO: Get the number of 0 columns and row and print
