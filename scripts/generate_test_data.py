from collections import defaultdict
import sys

input_file = open(sys.argv[1],'r')

state_dict = defaultdict(list)
end_state = 1

for line in input_file:
    tokens = line.split()
    if len(tokens) == 5:
       state_dict[int(tokens[0])].append((tokens[1],tokens[2],tokens[3]))

class tree_node(object):
    def __init__(self):
        self.string_info = list()
        self.state_info = list()

def get_string_permutations(state_dict,state,composed_str):
    if state == -1 or state_dict[state]== [] or len(state_dict[state]) == 1:
        print str(composed_str)
    else:
        #print len(state_dict[0])
        for nodes in state_dict[state]:
            if state == 0:
	        print "Processing "+str(nodes[0])+" "+str(nodes[1])+" "+str(nodes[2])
                get_string_permutations(state_dict,int(nodes[0]),composed_str+str(nodes[2]))
                #yield the_str
            elif int(nodes[0]) == 1:
                #print "Processing "+str(nodes[0])+" "+str(nodes[1])+" "+str(nodes[2])
                get_string_permutations(state_dict,int(-1),composed_str+str(nodes[2]))
                #yield the_str
                #str(nodes[2])+" "+get_string_permutations(state_dict,int(-1))
            else:
	        print "Processing "+str(nodes[0])+" "+str(nodes[1])+" "+str(nodes[2])
                #print "Calling "+str(nodes[0])
                get_string_permutations(state_dict,int(nodes[0]),composed_str+str(nodes[2]))
                #print the_str
                #yield the_str
                #str(nodes[2])+" "+get_string_permutations(state_dict,int(nodes[0]))


#Try to build the tree here
string_tree = list()
string_tree.append(list())
for pairs in state_dict[0]:
    string_tree[0].append(pairs)

#a = get_string_permutations2(state_dict,0,"")

list_accept = []
list_track = []
state_track = dict()

for nodes in state_dict[0]:
    if int(nodes[0]) == 1:
        print nodes[1]
        list_accept.append((int(nodes[0]),str(nodes[1]),1))
    else:
        list_track.append((int(nodes[0]),str(nodes[1]),1))
    state_track[0] = 1

while len(list_track) > 0:
    curr_elem = list_track.pop()
    for nodes in state_dict[curr_elem[0]]:
        if int(curr_elem[2]) < 80 and curr_elem[0] not in state_track:
            if int(nodes[0]) == 1:
                print str(curr_elem[1])+" "+str(nodes[1])
                list_accept.append((int(nodes[0]),str(curr_elem[1])+" "+str(nodes[1]),int(curr_elem[2]+1)))
            else:
                list_track.append((int(nodes[0]),str(curr_elem[1])+" "+str(nodes[1]),int(curr_elem[2]+1)))
    state_track[curr_elem[0]] = 1
