#This is a file to plot the sparsity in a transducer/graph
from __future__ import division
import matplotlib
matplotlib.use('Agg')

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
#import matplotlib
#matplotlib.use('Agg')
import sys

f = open(sys.argv[1],'r')

max_source = 0
max_target = 0

for line in f:
	tokens = line.split()
	source = int(tokens[0])
	target = int(tokens[1])
	if source > max_source:
		max_source = source
	if target > max_target:
		max_target = target

num_states = max(max_target,max_source)
num_states+=1
state_matrix = np.zeros((num_states, num_states))

f2 = open(sys.argv[1],'r')
for line in f2:
	tokens = line.split()
	source = int(tokens[0])
	target = int(tokens[1])
	#print("Source "+str(source)+" and target "+str(target)+" = "+str(state_matrix[source][target]))
	state_matrix[source][target]+=1



plt.imshow(state_matrix, interpolation='nearest')
plt.savefig('books_read2.png')
#plt.show()
