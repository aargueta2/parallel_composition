import sys

file_serial = open(sys.argv[1],'r')
file_parallel = open(sys.argv[2],'r')
check_dict = dict()

for line in file_serial:
	tokens = line.split()
        if tokens[5] not in check_dict:
		check_dict[tokens[5]] = dict()
	if tokens[7] not in check_dict[tokens[5]]:
		check_dict[tokens[5]][tokens[7]] = dict()
	if tokens[9] not in check_dict[tokens[5]][tokens[7]]:
		check_dict[tokens[5]][tokens[7]][tokens[9]] = dict()
	if tokens[11] not in check_dict[tokens[5]][tokens[7]][tokens[9]]:
		check_dict[tokens[5]][tokens[7]][tokens[9]][tokens[11]] = dict()
	if tokens[14] not in check_dict[tokens[5]][tokens[7]][tokens[9]][tokens[11]]:
		check_dict[tokens[5]][tokens[7]][tokens[9]][tokens[11]][tokens[14]] = dict()
	if tokens[17] not in check_dict[tokens[5]][tokens[7]][tokens[9]][tokens[11]][tokens[14]]:
		check_dict[tokens[5]][tokens[7]][tokens[9]][tokens[11]][tokens[14]][tokens[17]] = dict()
	if tokens[20] not in check_dict[tokens[5]][tokens[7]][tokens[9]][tokens[11]][tokens[14]][tokens[17]]:
		check_dict[tokens[5]][tokens[7]][tokens[9]][tokens[11]][tokens[14]][tokens[17]][tokens[20]] = 1
file_serial.close()

for line in file_parallel:
	tokens = line.split()
	#print tokens
	if check_dict[tokens[5]][tokens[7]][tokens[9]][tokens[11]][tokens[14]][tokens[17]][tokens[20]] != 1:
		print("ERROR")


file_parallel.close()


