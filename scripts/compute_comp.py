import sys
fst1 = open(sys.argv[1],'r')
fst2 = open(sys.argv[2],'r')

dict1 = dict()
dict2 = dict()

for line in fst1:
	tokens = line.split()
	if len(tokens) > 3:
		output = tokens[3]
		if output in dict1:
			dict1[output] = dict1[output] +1
		else:
			dict1[output] = int(1)
fst1.close()

for line in fst2:
	tokens = line.split()
        if len(tokens) > 3:
                output = tokens[2]
                if output in dict2:
                        dict2[output] = dict2[output] +1
                else:
                        dict2[output] = int(1)

comp_val = 0
for key in dict1:
	if key in dict2:
		print "key: "+str(key)+" and counts: "+str(dict1[key])+" "+str(dict2[key])
		comp_val += dict1[key] * dict2[key]
print comp_val

