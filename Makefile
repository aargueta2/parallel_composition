#CXX=clang++
#CUDA=/usr/local/cuda
#NVCC=$(CUDA)/bin/nvcc
NVCC=nvcc
GPP=g++

CXXFLAGS=-std=c++11 -I./common -O3 -I /afs/crc.nd.edu/group/nlp/software/boost/1.56.0/boost_1_56_0
NVCCFLAGS=--use_fast_math -lineinfo -Wno-deprecated-gpu-targets -DTHRUST_DEBUG #-Xcompiler -fopenmp

TARGETS=./bin/composition ./bin/composition_linear ./bin/composition_serial ./bin/composition_fast #./bin/composition_cl
COMMON=./common/numberizer.o ./common/fst.o

all: $(TARGETS)

install: composition
	cp $(TARGETS) ./bin/

%.o: %.cu
	$(NVCC) -x cu $(CXXFLAGS) $(NVCCFLAGS) -dc $< -o $@

./bin/composition: composition.o $(COMMON)
	$(NVCC) $(NVCCFLAGS) $(LDFLAGS) $^ -o $@

./bin/composition_fast: composition_fast.o $(COMMON)
	$(NVCC) $(NVCCFLAGS) $(LDFLAGS) $^ -o $@

./bin/composition_linear: composition_linear.o $(COMMON)
	$(NVCC) $(NVCCFLAGS) $(LDFLAGS) $^ -o $@

./bin/composition_cl: composition_cl.cpp $(COMMON)
	$(GPP) $(CXXFLAGS) $^ -o $@ -lOpenCL

./bin/composition_serial: composition_serial.cpp $(COMMON)
	$(GPP) $(CXXFLAGS) $^ -o $@

clean:
	rm -f *.o $(COMMON) $(TARGETS)

