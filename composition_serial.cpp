/***
  Author: Arturo Argueta
***/

#include <fstream>
#include <string>
#include <chrono>
#include <iterator>
#include <queue>
#include <ctime>
#include <map>

#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_fst_opencl.hpp"
#include "composition_serial.hpp"

#define DEBUG 1
#define PRINT_RES 0

using namespace std;

int ceildiv(size_t x, size_t y) { return (x-1)/y+1; }

int main (int argc, char *argv[]) {

  #if DEBUG
    printf("Number of args %d \n",argc);
  #endif

  if(argc != 7){
    cout << "Command: " << argv[0] << " input_map output_map fst_file input_map output_map fst_file composition_string"<< endl;
    exit(0);
  }

  #if DEBUG
    printf("number of arguments is %d \n",argc);
  #endif

  auto clock1 = std::chrono::steady_clock::now();

  #if DEBUG
    printf("Reading First FST \n");
  #endif

  //Read first transducer
  const numberizer inr = read_numberizer(argv[1]);
  const numberizer onr = read_numberizer(argv[2]);
  input_fst m = read_fst(argv[3], inr, onr);

  #if DEBUG  
    printf("Reading Second FST \n");
  #endif

  //Read second transducer  
  const numberizer inr2 = read_numberizer(argv[4]);
  const numberizer onr2 = read_numberizer(argv[5]);
  output_fst m2 = read_fst(argv[6], inr2, onr2);

  //int memory_pool = ceildiv(size_t(sizeof(composed_fst_node)));
  //composed_fst test = composed_fst(memory_pool);

  long int memory_pool_elements = m2.num_states * m.num_states * 3;//5;

  //printf("The number of states is %lu \n",memory_pool_elements);
  memory_pool_elements = 1900000000;
  #if DEBUG
    printf("The number of states is %d and %d and alloc %d \n",m.probs.size(),m2.probs.size(), memory_pool_elements);
  #endif

  compact_fst_node* states;
  states = (compact_fst_node*)malloc(memory_pool_elements *sizeof(compact_fst_node));
  #if DEBUG
    printf("State memory allocated \n");
  #endif

  auto clock2 = std::chrono::steady_clock::now();
  std::clock_t c_start = std::clock();
  unsigned long long int ret_value;

  std::map<compact_fst_node,float> composed_fst_map;

  for (int iteration=0; iteration< 1; iteration++) {

      ret_value = compute_efficient_composition(m,m2,states);//statelist);

      /*
      for(unsigned long long int i=0; i<ret_value; i++){
       //composed_fst_map[states[i]] += states[i].prob;
        //printf("The value is %f \n",composed_fst_map[states_host[i]]);
      }
      */

  }


  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FSTs: " << diff.count() << endl;

   
  auto clock3 = std::chrono::steady_clock::now();
  std::clock_t c_end = std::clock();

  long double time_elapsed_ms = 1000.0 * (c_end-c_start) / CLOCKS_PER_SEC;
  //std::cout << "CPU time used: " << time_elapsed_ms << " ms\n";
  std::chrono::duration<double> diff_compose = clock3-clock2;
  cout << "Time to compose FSTs: " << diff_compose.count() << endl;


  printf("The number of copied states is %d \n",int(ret_value));
  

   #if PRINT_RES
  //Print the states to the console
  for(unsigned long long int i=0; i<ret_value; i++){
    compact_fst_node iter_node = states[i];
    int from_state = iter_node.from_state; 
    int to_state = iter_node.to_state;
    int output = iter_node.output;
    int input = iter_node.input;
    float prob = iter_node.prob;

    cout << "The node " << i << " goes from " << from_state << " to " << to_state << " with input " << inr.num_to_word(input) << " and output " << onr2.num_to_word(output) << " and prob " << prob << endl;
  }
  #endif

  free(states);
  return 0;
} 
