#include <fstream>
#include <string>
#include <chrono>

#include <fst/fstlib.h>
#include <fst/vector-fst.h>
#include <fst/string.h>

using namespace fst;
using namespace std;

int main(int argc, char* argv[])
{
  // Reads in an input FST. 
  //StdVectorFst *input_text = StdVectorFst::Read("input_large.fst.bin");

  // Reads in an input FST.
  //StdVectorFst *input = StdVectorFst::Read("/afs/crc.nd.edu/group/nlp/01/aargueta/data/transducers_just/europarl/10000/de/de-en/de-en-de.fst.bin_sorted");
  StdVectorFst *input = StdVectorFst::Read(argv[1]);

  // Reads in the transduction model. 
  //StdVectorFst *model = StdVectorFst::Read("/afs/crc.nd.edu/group/nlp/01/aargueta/data/transducers_just/europarl/10000/de/en-de/de-en-de.fst.bin");
  StdVectorFst *model = StdVectorFst::Read(argv[2]);

  // The FSTs must be sorted along the dimensions they will be joined.
  // In fact, only one needs to be so sorted.
  // This could have instead been done for "model.fst" when it was created. 
  //ArcSort(input_text, StdOLabelCompare())
;
  //ArcSort(input, StdOLabelCompare());
  //ArcSort(model, StdILabelCompare());

  // Container for composition output. 
  StdVectorFst result;
  StdVectorFst output;

  auto clock1 = std::chrono::steady_clock::now();

  //for(int x=0;x<1000;x++){
    Compose(*input, *model, &result);
    StateMap(result, &output, ArcSumMapper<StdArc>(result));
  //}

  //ArcSort(&result, StdOLabelCompare());
  //Compose(*(&result), *model, &output); 


  // Just keeps the output labels. 
  //Project(&output, PROJECT_OUTPUT);
  auto clock2 = std::chrono::steady_clock::now();


  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to compose FST: " << diff.count() << endl;

  // Writes the result FST to a file.
  //output.Write("result.fst");

}
