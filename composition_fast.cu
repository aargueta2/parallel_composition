/***
  Author: Arturo Argueta
***/

#include <fstream>
#include <string>
#include <chrono>
#include <iterator>
#include <queue>

#include <map>
#include <string>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>

typedef std::tuple<int,int,int,int,int,int> composed_edge;


#include <cuda.h>
#include <cuda_runtime_api.h>
#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_fst.hpp"
#include "prob_ptr.hpp"
#include "composition_fast.hpp"
#define PRINT_RES 0

using namespace std;

int ceildiv(size_t x, size_t y) { return (x-1)/y+1; }

int main (int argc, char *argv[]) {
  cudaError err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }

  if(argc != 7){
    cout << "Command: " << argv[0] << " input_map output_map fst_file input_map output_map fst_file composition_string"<< endl;
    exit(0);
  }

  #if DEBUG
    printf("number of arguments is %d \n",argc);
  #endif
  auto clock1 = std::chrono::steady_clock::now();

  #if DEBUG
    printf("Reading First FST \n");
  #endif


  //Read first transducer
  const numberizer inr = read_numberizer(argv[1]);
  const numberizer onr = read_numberizer(argv[2]);
  input_fst m = read_fst(argv[3], inr, onr);

  #if DEBUG
    printf("Reading Second FST \n");
  #endif

  //Read second transducer  
  const numberizer inr2 = read_numberizer(argv[4]);
  const numberizer onr2 = read_numberizer(argv[5]);
  output_fst m2 = read_fst(argv[6], inr2, onr2);

  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() transducer failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }


  #if DEBUG
    size_t f1, t1;
    cudaMemGetInfo(&f1, &t1); // Returns the amount of free memory and taken memory
    printf("The number of states is %d %d and input %d output %d \n",m.num_states,m2.num_states,m.num_inputs,m2.num_outputs);
    printf("Free: %zu and taken %zu \n",f1,t1);
  #endif

  int memory_pool = ceildiv(size_t(1100000000),size_t(sizeof(compact_fst_node)));// For this method, we allocate ~10 GB
  memory_pool = 1900000 * 3;
  fst_and_keys test = fst_and_keys(memory_pool);

 
  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() Memory Pool failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }

  sort_pointers sort_struct = sort_pointers(memory_pool);
  size_t f, t;
  cudaMemGetInfo(&f, &t); // Returns the amount of free memory and taken memory

  #if DEBUG
    printf("Free: %zu and total %zu \n",f,t);
  #endif

  compact_fst_node* states_host;
  unsigned int host_alloc_elements;
  host_alloc_elements = 1900000000;
  checkCuda(cudaMallocHost((void**)&states_host, (host_alloc_elements  *sizeof(compact_fst_node))));
  //states_host = (composed_fst_node*)malloc(host_alloc_elements  *sizeof(composed_fst_node));


  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() state host malloc failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }

  #if DEBUG
    printf("The amount of allocated slots is %d \n",int(m2.num_states * m.num_states * 4.5));
  #endif

  hash_element *dev_hash_table;
  cudaMalloc( (void**)&dev_hash_table, int(m2.num_states * m.num_states) * sizeof(hash_element));
  cudaMemset(dev_hash_table,0,int(m2.num_states * m.num_states) * sizeof(hash_element));

  state_tup *vertex_frontier;
  cudaMalloc( (void**)&vertex_frontier,int(m2.num_states * m.num_states) * sizeof(state_tup));

  int *dev_num_states;
  cudaMalloc( (void**)&dev_num_states, sizeof(int));
 
  state_tup* stack_list;
  stack_list = (state_tup*)malloc(m2.num_states*10);

  #if DEBUG
    printf("Stack list size %d \n",(m2.num_states*10));
    printf("The number of allocated states is %d probs \n",int(m.num_inputs * m2.num_outputs));
  #endif


  std::map<short, float> composed_fst_map;


  int* prob_table;
  cudaMalloc( (void**)&prob_table,(int(m.num_inputs * m2.num_outputs) + int(m.num_states)) * sizeof(int));   
  //cudaMemset(prob_table,0,int(m.num_inputs * m2.num_outputs) * sizeof(float));

  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() mallochost failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }

  #if DEBUG
    printf("Composed fst created \n");
  #endif

  cudaStream_t streams[m.num_outputs];
  for(int i = 0; i< m.num_outputs; i++){
    cudaStreamCreate(&streams[i]);
  }


  #if DEBUG
    size_t f2, t2;
    cudaMemGetInfo(&f2, &t2); // Returns the amount of free memory and taken memory
    printf("The amount of free memory needed: %zu \n",(host_alloc_elements  *sizeof(compact_fst_node)));
    printf("Free: %zu and taken %zu \n",f2,t2);
  #endif



  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() stream failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }
  cudaDeviceSynchronize();

  //Start timing
  auto clock2 = std::chrono::steady_clock::now();
  unsigned long long int ret_value;
  for (int iteration=0; iteration< 1; iteration++) {
      ret_value = compute_efficient_composition(m,m2,test, states_host,&streams[0],dev_hash_table,vertex_frontier,dev_num_states,stack_list,sort_struct,prob_table);
  }

  //Finish timing
  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FSTs: " << diff.count() << endl;

  auto clock3 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff_compose = clock3-clock2;
  cout << "Time to compose FSTs: " << diff_compose.count() << endl;

  for(int i = 0; i< m.num_outputs; i++){
    cudaStreamDestroy(streams[i]);
  }

  printf("The number of copied states is %d \n",int(ret_value));

  #if PRINT_RES
  //Print the states to the console
  for(unsigned long long int i=0; i<ret_value; i++){
    compact_fst_node iter_node = states_host[i];
    int from_state = iter_node.from_state;
    int to_state = iter_node.to_state;
    int output = iter_node.output;
    int input = iter_node.input;
    float prob = iter_node.prob;

    cout << "The node " << i << " goes from " << from_state << " to " << to_state << " with input " << inr.num_to_word(input) << " and output " << onr2.num_to_word(output) << " and prob " << prob << endl;
  }
  #endif

  test.free_structure();

  cudaFreeHost(states_host);
  free(stack_list);
  cudaFree(prob_table);
  cudaFreeHost(states_host);
  cudaFree(dev_hash_table);
  cudaFree(vertex_frontier);
  cudaFree(dev_num_states);

  return 0;
} 
