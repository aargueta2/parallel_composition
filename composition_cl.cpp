/***
  Author: Arturo Argueta
***/

#include <fstream>
#include <string>
#include <chrono>
#include <iterator>
#include <queue>

#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_fst_opencl.hpp"
#include "composition_opencl.hpp"

#define DEBUG 0
using namespace std;

int main (int argc, char *argv[]) {

  printf("Number of args %d \n",argc);
  if(argc != 7){
    cout << "Command: " << argv[0] << " input_map output_map fst_file input_map output_map fst_file composition_string"<< endl;
    exit(0);
  }

  printf("number of arguments is %d \n",argc);
  auto clock1 = std::chrono::steady_clock::now();

  printf("Reading First FST \n");
  //Read first transducer

  const numberizer inr = read_numberizer(argv[1]);
  const numberizer onr = read_numberizer(argv[2]);
  input_fst m = read_fst(argv[3], inr, onr);

  
  printf("Reading Second FST \n");
  //Read second transducer  
  const numberizer inr2 = read_numberizer(argv[4]);
  const numberizer onr2 = read_numberizer(argv[5]);

  output_fst m2 = read_fst(argv[6], inr2, onr2);



  //create variable to keep track of the nodes expanded
  //printf("allocating %d \n",(m.num_outputs * m2.num_inputs));
  composed_fst test = composed_fst(m.num_states * m2.num_states * 8);
  //printf("The number of states is %d \n",(m2.num_states*m.num_states*8));

  vector<int> input_string,output_vector_host;

  auto clock2 = std::chrono::steady_clock::now();
  for (int iteration=0; iteration< 1; iteration++) {
      compute_efficient_composition(m,m2,test);
  }


  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FSTs: " << diff.count() << endl;

  //copy_fst_back(test,(m2.num_states*m.num_states*8));
   
  auto clock3 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff_compose = clock3-clock2;
  cout << "Time to compose FSTs: " << diff_compose.count() << endl;

  return 0;
}

