/***
  Author: Arturo Argueta
***/

#include <fstream>
#include <string>
#include <chrono>
#include <iterator>
#include <queue>

#include <cuda.h>
#include <cuda_runtime_api.h>
#include "numberizer.hpp"
#include "fst.hpp"
#include "gpu_fst.hpp"
#include "prob_ptr.hpp"
#include "composition.hpp"

#define DEBUG 0
using namespace std;

int ceildiv(size_t x, size_t y) { return (x-1)/y+1; }

int main (int argc, char *argv[]) {

  cudaError err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }

  printf("Number of args %d \n",argc);
  if(argc != 7){
    cout << "Command: " << argv[0] << " input_map output_map fst_file input_map output_map fst_file composition_string"<< endl;
    exit(0);
  }

  printf("number of arguments is %d \n",argc);
  auto clock1 = std::chrono::steady_clock::now();

  printf("Reading First FST \n");
  //Read first transducer
  const numberizer inr = read_numberizer(argv[1]);
  const numberizer onr = read_numberizer(argv[2]);
  input_fst m = read_fst(argv[3], inr, onr);

  
  printf("Reading Second FST \n");
  //Read second transducer  
  const numberizer inr2 = read_numberizer(argv[4]);
  const numberizer onr2 = read_numberizer(argv[5]);
  output_fst m2 = read_fst(argv[6], inr2, onr2);

  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() transducer failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }

  //create variable to keep track of the nodes expanded
  //printf("allocating %d \n",(m.num_outputs * m2.num_inputs));


  size_t f1, t1;
  cudaMemGetInfo(&f1, &t1); // Returns the amount of free memory and taken memory
  printf("Free: %zu and taken %zu \n",f1,t1);

  int memory_pool = ceildiv(size_t(1100000000),size_t(sizeof(composed_fst_node)));// For this method, we allocate ~10 GB
  memory_pool = 3000000;
  //printf("Allocating %d states \n",memory_pool);
  composed_fst test = composed_fst(memory_pool);//m.num_states * m2.num_states * 3);
  //printf("The number of states is %d \n",(m2.num_states*m.num_states*8));

  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() Memory Pool failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }

  size_t f, t;
  cudaMemGetInfo(&f, &t); // Returns the amount of free memory and taken memory
  printf("Free: %zu and taken %zu \n",f,t);
  //std::list<composed_fst_node> statelist;

  composed_fst_node* states_host;
  //states_host = (composed_fst_node*)malloc( m2.num_states * m.num_states  *sizeof(composed_fst_node)* 4.5);


  printf("Allocating host %f and state %d \n",(m2.num_states * m.num_states * 4.5),m2.num_states);
  unsigned int host_alloc_elements = 1737254290;
  host_alloc_elements =  3000000;//m2.num_states * m.num_states;

  cudaMallocHost((void**)&states_host, host_alloc_elements *sizeof(composed_fst_node) * 4.5);
  printf("The amount of allocated slots is %d \n",int(m2.num_states * m.num_states * 4.5));

  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() mallochost failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }

  printf("Composed fst created \n");

  //thrust::device_vector<int> output_vector_string;
  //vector<int> input_string,output_vector_host;

  cudaStream_t streams[m.num_outputs];

  for(int i = 0; i< m.num_outputs; i++){
    cudaStreamCreate(&streams[i]);
  }

  err = cudaGetLastError();
  if ( cudaSuccess != err )
  {
    printf("cudaCheckError() stream failed at %s\n",cudaGetErrorString( err ) );
    exit( -1 );
  }
  cudaDeviceSynchronize();
  auto clock2 = std::chrono::steady_clock::now();
  unsigned long long int ret_value;
  for (int iteration=0; iteration< 1; iteration++) {

      ret_value = compute_efficient_composition(m,m2,test, states_host,&streams[0]);//statelist);
      //compute_start_state_composition(m,m2,test);
  }


  std::chrono::duration<double> diff = clock2-clock1;
  cout << "Time to read FSTs: " << diff.count() << endl;

  //thrust::copy(output_vector_string.begin(), output_vector_string.end(), output_vector_host.begin());
  //copy_fst_back(test,(m2.num_states*m.num_states*8));
   
  auto clock3 = std::chrono::steady_clock::now();
  std::chrono::duration<double> diff_compose = clock3-clock2;
  cout << "Time to compose FSTs: " << diff_compose.count() << endl;

  for(int i = 0; i< m.num_outputs; i++){
    cudaStreamDestroy(streams[i]);
  }

/*
  //Print the states to the console
  for(unsigned long long int i=0; i<ret_value; i++){
    composed_fst_node iter_node = states_host[i];
    int to_state = iter_node.to_state1;
    int to_state2 = iter_node.to_state2;
    int output = iter_node.output;
    int input = iter_node.input;
    float prob = iter_node.prob;         
    
    cout << "The node " << i << " goes to " << to_state << " and " << to_state2 << " with input " << inr.num_to_word(input) << " and output " << onr2.num_to_word(output) << " and prob " << prob << endl;
  }
*/
  printf("The number of copied states is %d \n",int(ret_value));
  cout << "Time to compose FSTs: " << diff_compose.count() << endl;
  cudaFreeHost(states_host);

  return 0;
} 
